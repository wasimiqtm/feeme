<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::group(['middleware' => 'api',], function() {
    Route::post('login', 'API\AuthController@login');
    Route::post('signup', 'API\AuthController@signup');
    Route::post('logout', 'API\AuthController@logout');
    Route::post('store', 'API\CallSmsController@storeData');
    Route::put('update_password', 'API\AuthController@updatePassword');
    Route::resource('test', 'test');
    // Route::resource('test', 'test');

    /**************** ACCOUNT ROUTES ********************/
    Route::get('accounts/get_account_detail/{accountId}', 'API\AccountController@getSingleAccountDetail');
    Route::get('accounts/get_account_payment_methods/{accountId}', 'API\AccountController@getAccountPaymentMethod');
    Route::get('accounts/{offset}/{limit}', 'API\AccountController@index');
    Route::delete('accounts/{account_id}', 'API\AccountController@destroy');
    Route::post('store_account_personal_detail', 'API\AccountController@storePersonalDetail');
    Route::put('store_account_company_detail/{accountId}', 'API\AccountController@storeCompanyDetail');
    Route::put('store_account_billing_detail/{accountId}', 'API\AccountController@storeBillingDetail');
    Route::post('get_account_subscriptions', 'API\AccountController@accountSubscriptions');
    
    /**************** ACCOUNT ROUTES ********************/

    /**************** INVOICE ROUTES ********************/
    Route::get('invoices/get_invoices/{accountId}', 'API\InvoiceController@getInvoices');
    Route::get('invoices/get_account_invoices_payment/{accountId}', 'API\InvoiceController@getAccountInvoicePayment');
    Route::post('invoices/create_charge', 'API\InvoiceController@addCharge');
    /**************** INVOICE ROUTES ********************/

    /**************** CATALOG ROUTES ********************/
    Route::get('catalog', 'API\CatalogController@index');
    /**************** SUBSCRIPTION ROUTES ********************/

    /*************** SUBSCRIPTION ROUTES **************/
    Route::get('subscription/get_account_bundle/{accountId}', 'API\SubscriptionController@getAccountBundle');
    Route::post('subscription/add_new_subscription', 'API\SubscriptionController@addSubscription');
    Route::post('subscription/add_subscription_with_add_on_products', 'API\SubscriptionController@addSubscriptionWithAddOnProducts');
    /*************** SUBSCRIPTION ROUTES ***************/

    /**************** PAYMENT ROUTES ********************/
    Route::get('payment/{account_id}', 'API\PaymentController@index');
    /**************** PAYMENT ROUTES ********************/

    /**************** TIMELIME ROUTES ********************/
    Route::get('timeline', 'API\TimelineController@index');
    /**************** TIMELIME ROUTES ********************/

    /**************** OVERDUE ROUTES ********************/
    Route::get('overdue/get_account_overdue/{accountId}', 'API\OverdueController@getAccountOverdue');
    /**************** OVERDUE ROUTES ********************/  

    /**************** CATALOG ROUTES ********************/
    Route::get('catalog', 'API\CatalogController@index');
    Route::get('catalog/get_base_plans', 'API\CatalogController@getBasePlans');
    Route::post('catalog/add_product', 'API\CatalogController@addProduct');
    /**************** CATALOG ROUTES ********************/
});


<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::group(['middleware' => 'api',], function() {
    Route::post('login', 'API\AuthController@login');
    Route::post('signup', 'API\AuthController@signup');
    Route::post('logout', 'API\AuthController@logout');
    Route::post('store_sms_call', 'API\CallSmsController@store_sms_call');
    Route::put('update_password', 'API\AuthController@updatePassword');
    Route::put('update_account/{accountId}', 'API\AccountController@updateAccount');
    Route::resource('test', 'test');
    // Route::resource('test', 'test');

    /**************** ACCOUNT ROUTES ********************/
    Route::get('accounts/get_account_detail/{accountId}', 'API\AccountController@getSingleAccountDetail');
    Route::get('accounts/get_account_payment_methods/{accountId}', 'API\AccountController@getAccountPaymentMethod');
    Route::get('accounts/{offset}/{limit}', 'API\AccountController@index');
    Route::delete('accounts/{account_id}', 'API\AccountController@destroy');
    Route::post('store_account_personal_detail', 'API\AccountController@storePersonalDetail');
    Route::put('store_account_company_detail/{accountId}', 'API\AccountController@storeCompanyDetail');
    Route::put('store_account_billing_detail/{accountId}', 'API\AccountController@storeBillingDetail');
    Route::post('get_account_subscriptions', 'API\AccountController@accountSubscriptions');
    /**************** ACCOUNT ROUTES ********************/

    /**************** INVOICE ROUTES ********************/
    Route::get('invoices/get_invoices/{accountId}', 'API\InvoiceController@getInvoices');
    Route::get('invoices/get_account_invoices_payment/{accountId}', 'API\InvoiceController@getAccountInvoicePayment');
    /**************** INVOICE ROUTES ********************/

    /*************** SUBSCRIPTION ROUTES **************/
    Route::get('subscription/get_account_bundle/{accountId}', 'API\SubscriptionController@getAccountBundle');
    Route::post('subscription/add_new_subscription', 'API\SubscriptionController@addSubscription');
    Route::post('subscription/add_subscription_with_add_on_products', 'API\SubscriptionController@addSubscriptionWithAddOnProducts');
    Route::delete('subscription/cancel_subscription', 'API\SubscriptionController@cancelSubscription');
    Route::put('subscription/uncancel_subscription', 'API\SubscriptionController@unCancelSubscription');
    /*************** SUBSCRIPTION ROUTES ***************/

    /**************** PAYMENT ROUTES ********************/
    Route::get('payment', 'API\PaymentController@index');
    /**************** PAYMENT ROUTES ********************/

    /**************** TIMELIME ROUTES ********************/
    Route::get('timeline', 'API\TimelineController@index');
    /**************** TIMELIME ROUTES ********************/

    /**************** OVERDUE ROUTES ********************/
    Route::get('overdue/get_account_overdue/{accountId}', 'API\OverdueController@getAccountOverdue');
    /**************** OVERDUE ROUTES ********************/  

    /**************** CATALOG ROUTES ********************/
    Route::get('catalog', 'API\CatalogController@index');
    Route::get('catalog/get_base_plans', 'API\CatalogController@getBasePlans');
    Route::post('catalog/add_product', 'API\CatalogController@addProduct');
    /**************** CATALOG ROUTES ********************/
});

