<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::group(['middleware' => 'api',], function() {

    Route::post('login', 'API\AuthController@login');
    Route::post('signup', 'API\AuthController@signup');
    Route::post('logout', 'API\AuthController@logout');
    Route::put('update_password', 'API\AuthController@updatePassword');
    Route::post('sendOtp', 'API\AuthController@sendOtp');
    Route::post('verifyOtp', 'API\AuthController@verifyOtp');


    Route::resource('test', 'test');
    // Route::resource('test', 'test');

    /**************** ACCOUNT ROUTES ********************/
    Route::get('accounts/get_account_detail/{accountId}', 'API\AccountController@getSingleAccountDetail');
    Route::get('accounts/get_account_payment_methods/{accountId}', 'API\AccountController@getAccountPaymentMethod');
    Route::get('accounts/{offset}/{limit}', 'API\AccountController@index');
    Route::get('accountsWithSubscriptions', 'API\AccountController@accountSubscriptions');
    Route::delete('accounts/{account_id}', 'API\AccountController@destroy');
    Route::post('store_account_personal_detail', 'API\AccountController@storePersonalDetail');
    Route::post('store_account_company_detail', 'API\AccountController@storeCompanyDetail');
    Route::put('store_account_billing_detail/{accountId}', 'API\AccountController@storeBillingDetail');
    // Route::post('get_account_subscriptions', 'API\AccountController@accountSubscriptions');
    Route::put('update_account/{accountId}', 'API\AccountController@updateAccount');
    Route::put('update_company_details/{accountId}', 'API\AccountController@updateCompanyDetail');
    Route::put('update_email_config', 'API\AuthController@updateEmailConfig');
    Route::get('get_logged_in_user', 'API\AuthController@loggedInUser');
    Route::get('company_details/{accountId}', 'API\AccountController@getCompanyDetails');

    Route::put('allocate_attendance', 'API\AccountController@allocateAttendance');


    /**************** ACCOUNT ROUTES ********************/

    /**************** INVOICE ROUTES ********************/
    Route::get('invoices/get_invoices/{accountId}', 'API\InvoiceController@getInvoices');
    Route::get('invoices/get_account_invoices_payment/{accountId}', 'API\InvoiceController@getAccountInvoicePayment');
    Route::post('invoices/create_charge', 'API\InvoiceController@addCharge');
    Route::get('invoices/invoice_details/{invoiceId}', 'API\InvoiceController@getInvoiceDetails');
    Route::get('invoices/get_all_invoices/{offset}/{limit}', 'API\InvoiceController@getAllInvoices');
    Route::post('invoices/add_custom_product', 'API\InvoiceController@addCustomInvoice');
    Route::post('invoices/add_custom_product_adhoc', 'API\InvoiceController@addCustomInvoiceAdhoc');

    
    Route::put('invoices/commit_invoice', 'API\InvoiceController@commitInvoice');

    /**************** INVOICE ROUTES ********************/

    /*************** SUBSCRIPTION ROUTES **************/
    Route::get('subscription/get_account_bundle/{accountId}', 'API\SubscriptionController@getAccountBundle');
    Route::post('subscription/add_new_subscription', 'API\SubscriptionController@addSubscription');
    Route::post('subscription/add_subscription_with_add_on_products', 'API\SubscriptionController@addSubscriptionWithAddOnProducts');
    Route::delete('subscription/cancel_subscription', 'API\SubscriptionController@cancelSubscription');
    Route::put('subscription/uncancel_subscription', 'API\SubscriptionController@unCancelSubscription');
    /*************** SUBSCRIPTION ROUTES ***************/

    

    /**************** PAYMENT ROUTES ********************/
    Route::get('payment/{account_id}', 'API\PaymentController@index');
    Route::get('payment/get_all_payments/{offset}/{limit}', 'API\PaymentController@getAllPayments');
    /**************** PAYMENT ROUTES ********************/

    /**************** TIMELIME ROUTES ********************/
    Route::get('timeline/{accountId}', 'API\TimelineController@index');
    /**************** TIMELIME ROUTES ********************/

    /**************** OVERDUE ROUTES ********************/
    Route::get('overdue/get_account_overdue/{accountId}', 'API\OverdueController@getAccountOverdue');
    /**************** OVERDUE ROUTES ********************/  

    /**************** CATALOG ROUTES ********************/
    Route::get('catalog', 'API\CatalogController@index');
    Route::get('catalog/get_base_plans', 'API\CatalogController@getBasePlans');
    Route::post('catalog/add_product', 'API\CatalogController@addProduct');
    Route::get('catalog/get_product/{subscriptionId}', 'API\CatalogController@getProduct');

    /**************** CATALOG ROUTES ********************/

    /*************** USAGE ROUTES *******************/
    Route::post('usage/add_new_usage', 'API\UsageController@addUsage');
    Route::post('usage/add_new_usage_test', 'API\UsageController@addUsageTest');

    Route::get('usage/get_usage_against_subscription/{subscriptionId}/{startDate}/{endDate}', 'API\UsageController@getUsageAgainstSubscription');
    Route::get('usage/get_usage_against_subscription_and_unit_type/{subscriptionId}/{unitType}', 'API\UsageController@getUsageAgainstSubscriptionAndUnitType');
    Route::get('usage/unauthentic_attendances', 'API\UsageController@unAuthenticAttendances');
    Route::put('usage/update_unallocated/{id}', 'API\UsageController@updateUnAllocated');


    /*************** USAGE ROUTES *******************/

    /*************** EMAIL ROUTES *******************/
    Route::get('usage/get_emails', 'API\EmailController@getEmails');
    /*************** EMAIL ROUTES *******************/


    /*************** REPORTS ROUTES *******************/
    Route::get('get_report/{reportName}/{smoothName?}/{startDate?}/{endDate?}/{format?}', 'API\ReportController@getReport');
    // Route::get('get_report/{reportName}/{startDate?}/{endDate?}/{smoothName?}/{format?}', 'API\ReportController@getReport');
    /*************** REPORTS ROUTES *******************/
});