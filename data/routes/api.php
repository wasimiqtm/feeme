<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::group(['middleware' => 'api',], function() {
    Route::post('login', 'API\AuthController@login');
    Route::post('signup', 'API\AuthController@signup');
    Route::post('logout', 'API\AuthController@logout');
    Route::resource('test', 'test');
    // Route::get('test', 'API\AccountController@storeTags');

    /**************** ACCOUNT ROUTES ********************/
    Route::get('accounts/get_account_detail/{accountId}', 'API\AccountController@getSingleAccountDetail');
    Route::get('accounts/{offset}/{limit}', 'API\AccountController@index');
    Route::delete('accounts/{account_id}', 'API\AccountController@destroy');
    Route::post('store_account_personal_detail', 'API\AccountController@storePersonalDetail');
    Route::put('store_account_company_detail/{accountId}', 'API\AccountController@storeCompanyDetail');
    Route::put('store_account_billing_detail/{accountId}', 'API\AccountController@storeBillingDetail');
    Route::post('get_account_subscriptions', 'API\AccountController@accountSubscriptions');
    
    /**************** ACCOUNT ROUTES ********************/

    /**************** SUBSCRIPTION ROUTES ***************/
    // Route::get('subscription', 'API\SubscriptionController@index');
    Route::get('subscription/get_account_bundle/{accountId}', 'API\SubscriptionController@getAccountBundle');
    Route::post('subscription/add_new_subscription', 'API\SubscriptionController@addSubscription');
    /**************** SUBSCRIPTION ROUTES ****************/

    /**************** INVOICE ROUTES ********************/
    Route::get('invoices/get_invoices/{accountId}', 'API\InvoiceController@getInvoices');
    /**************** INVOICE ROUTES ********************/

    /**************** PAYMENT ROUTES ********************/
    Route::get('payment', 'API\PaymentController@index');
    /**************** PAYMENT ROUTES ********************/

    /**************** TIMELIME ROUTES ********************/
    Route::get('timeline', 'API\TimelineController@index');
    /**************** TIMELIME ROUTES ********************/

    /**************** OVERDUE ROUTES ********************/
    Route::get('overdue/get_account_overdue/{accountId}', 'API\OverdueController@getAccountOverdue');
    /**************** OVERDUE ROUTES ********************/  

    /**************** CATALOG ROUTES ********************/
    Route::get('catalog/get_base_plans', 'API\CatalogController@getBasePlans');
    Route::get('catalog/get_products', 'API\CatalogController@getProducts');
    Route::post('catalog/add_product', 'API\CatalogController@addProduct');
    /**************** CATALOG ROUTES ********************/
});
