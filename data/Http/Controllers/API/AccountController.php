<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
//FOR GUZZLE
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;

class AccountController extends Controller
{
    public $successStatus   = 200;
    public $errorStatus     = 401;
	private $headers = [
                            'Accept'                => 'application/json',
                            'Content-Type'          => 'application/json',
                            'X-Killbill-CreatedBy'  => 'Fee Me Web',
                       ];
    private $credential   = [];//'admin', 'password'

    public function __construct(){
        $this->headers['X-Killbill-ApiKey']     = env('API_KEY');
        $this->headers['X-Killbill-ApiSecret']  = env('API_SECRET');
    }
    public function index($offset, $limit){

    	$headers = apache_request_headers();
        $userCred = getUserFromToken($headers['token']);

        // return $userCred;
    	// return $this->headers;
		$this->credential[0] = $userCred[0];
		$this->credential[1] = $userCred[1];

    	$endpoint = env('API_BASE_URL').'/accounts/pagination?offset='.$offset.'&limit='.$limit;
    
    	try{

    		$client = new Client();
        	$response = $client->get($endpoint, ['auth'=> $this->credential, 'headers'=> $this->headers]);
        	$data['data'] 				= json_decode($response->getBody()->getContents(), true);

        	$data['total_records'] 		= $response->getHeader('x-killbill-pagination-totalnbrecords')[0];
        	$data['next_page_url'] 		= @$response->getHeader('x-killbill-pagination-nextpageuri')[0];
        	$data['max_records_showing'] = $response->getHeader('x-killbill-pagination-maxnbrecords')[0];
        	$data['current_offset'] 	= $response->getHeader('x-killbill-pagination-currentoffset')[0];
        	$data['next_offset'] 		= @$response->getHeader('x-killbill-pagination-nextoffset')[0];

        	return response()->json(['result' => $data], $response->getStatusCode());
    	}catch(ClientException $e){

    		$response = $e->getResponse();
            $data['status'] = false;
            $content = json_decode($response->getBody()->getContents(), true);
            $data['message'] = (!empty($content["message"])) ? $content["message"] : 'Some exception occured.';
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
    	}
    }


    public function storePersonalDetail(Request $request){

        $userCred = getUserFromToken($request->header('token'));
    	$this->credential[0] = $username = $userCred[0];
		$this->credential[1] = $password = $userCred[1];

        // return $request->all();
    	$bodyData = [
					"name" 				=> $request->firstName.' '.$request->middleName.' '.$request->lastName,
					"firstNameLength" 	=> strlen($request->firstName),
					"externalKey" 		=> (!empty($request->idNumber) ? $request->idNumber : ''),
					"email" 			=> $request->email,
					"address1" 			=> $request->addressLineOne,
					"address2" 			=> $request->addressLineTwo,
					// "postalCode" 		=> $requestData->name,
					// "company" 			=> $requestData->name,
					"city" 				=> $request->addressCity,
					"state" 			=> $request->province,
					"country" 			=> $request->country,
					// "locale" 			=> $request->name,
					"phone" 			=> $request->mobile,
					"notes" 			=> $request->notes,
    	];
    	$requestOptions = [
    			'auth'	=> [$username, $password],
            	'headers' => $this->headers,
            	'json' => $bodyData,
             ];

        $endpoint = env('API_BASE_URL').'/accounts';
    	try{
    		$client = new Client();
            $response = $client->post($endpoint, $requestOptions);  

            $message = "Account created successfully";
            if ((int) $response->getStatusCode() === 201) { 
                $locationHeader = $response->getHeader('location')[0];
                $locationHeaderArray = explode('/', $locationHeader);
                $accountId = end($locationHeaderArray);

                if(!empty($request->tags)){
                    $tagsReponse = $this->storeTags($accountId, $request->tags, $this->credential);
                    $message .= $tagsReponse['message'];
                }

            	/*if ($response->hasHeader('location')) {
            		# code...
            		$locationHeader = $response->getHeader('location')[0];
            		$locationHeaderArray = explode('/', $locationHeader);
            		$accountId = end($locationHeaderArray);

            		
            	}*/
                $data['status'] = true;
                $data['message'] = $message;
                $data['data'] = $accountId;

                return response()->json(['result' => $data], $response->getStatusCode());
            } 
        	
    	}catch(ClientException $e){
    		$response = $e->getResponse();
            $data['status'] = false;
            $content = json_decode($response->getBody()->getContents(), true);
            $data['message'] = ($content["message"])? $content["message"]: "Account creation failed.";
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
    	}
    }

    public function storeTags($accountId, $tags, $credential){
    	$endpoint = env('API_BASE_URL').'/tagDefinitions';
    	
    	$tagsDefinition = [];
    	try{
    		// return 'hello';
    		$client = new Client(); //
        	$response = $client->get( $endpoint, ['auth'=> $credential, 'headers'=> $this->headers]);
            $response = json_decode($response->getBody()->getContents());

            /************* MAKING ARRAY OF ALL AVAILABLE SERVER TAGS ***************/
            foreach ($response as $key => $value) {
            	$tagsDefinition[$value->name] = $value->id;
            }
            /************* MAKING ARRAY OF ALL AVAILABLE SERVER TAGS ***************/
            
            /***************** GETTING TAGS IDS *************************/
            $tagList = [];
            /*print_r($tags);
            print_r($tagsDefinition);*/
            foreach ($tags as $key => $value) {
            	$tagList[] = $tagsDefinition[$value['name']];
            }
            /***************** GETTING TAGS IDS *************************/
            // return implode(',', $tagList);


            /***************** ATTACHING TAGS TO ACCOUNT ***********************/
            $requestOptions = [
    			'auth'	=> $this->credential,
            	'headers' => $this->headers
             ];
            $tagListString = implode(',', $tagList);
            $addTagEndPoint = env('API_BASE_URL')."/accounts/{$accountId}/tags?tagList=".$tagListString;
            // return $addTagEndPoint;
            $postResponse = $client->post($addTagEndPoint, $requestOptions);

            if ($postResponse->getStatusCode() === 201) {
	            $data['status'] = true;
	            $data['message'] = ", Tag/s added to account.";
            	return $data;
            }


    	}catch(ClientException $e){
    		$response = $e->getResponse();
            $data['status'] = false;
            $data['message'] = ", Tag/s not added to account.";

            return $data;
    	}/*catch(GuzzleException $e){
    		dd($e);

            return $data['message'];
    	}*/
    }



    /**************** COMPANY DETAIL ********************/
    function storeCompanyDetail(Request $request, $accountId){
        $userCred = getUserFromToken($request->header('token'));
        $this->credential[0] = $username = $userCred[0];
        $this->credential[1] = $password = $userCred[1];

       $endpoint = env('API_BASE_URL')."/accounts/$accountId?treatNullAsReset=false";
       $bodyData = [
                    "company"               => $request->companyName,
                    "currency"              => $request->currency,
                    "timeZone"              => 'UTC',//$request->timeZone,
                    // "registeredName"    => $request->registeredName,
                    // "registrationNo"    => $request->companyRegistrationNo,
                    // "companyType"       => strlen($request->companyType),
                    // "industry"          => strlen($request->industary),
                    // "vatRegistration"   => $request->vatRegistrationNo,
                    // "website"           => $request->website,
                    // "billDomain"        => $request->billableDomain,
                    // "mobile"            => $request->mobile,
                    // "landline"          => $request->landLine,
                    // "other"             => $request->otherPhone,
                    // "addressLineOne"    => $request->addressLineOne,
                    // "addressLineTwo"    => $request->addressLineTwo,
                    // "subrub"            => $request->subrub,
                    // "city"              => $request->city,
                    // "country"           => $request->country,
                    // "state"             => $request->province,
                    // "notes"             => $request->notes,
        ];
        // return $endpoint;
        $requestOptions = [
                'auth'  => [$username, $password],
                'headers' => $this->headers,
                'json' => $bodyData,
             ];
        try{
            $client = new Client();
            $response = $client->put($endpoint, $requestOptions);
            $message = "Account updated successfully";
            
            $data['status'] = true;
            $data['message'] = $message;
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
            
        }catch(ClientException $e){
            $response = $e->getResponse();
            $data['status'] = false;
            $content = json_decode($response->getBody()->getContents(), true);
            $data['message'] = ($content["message"])? $content["message"]: "Account updation failed.";
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
        
    }
    /**************** COMPANY DETAIL ********************/

    /**************** BILLING DETAIL ********************/
    function storeBillingDetail(Request $request, $accountId){
        $userCred = getUserFromToken($request->header('token'));
        $this->credential[0] = $username = $userCred[0];
        $this->credential[1] = $password = $userCred[1];

        $endpoint = env('API_BASE_URL')."/accounts/$accountId?treatNullAsReset=false";
       $bodyData = [
                    // "billCycleDayLocal"     => $request->billingCycleDay,
                    // "billingDomain"         => $request->billingDomain,
                    // "billingTime"           => $request->billingTime,
                    // "currency"              => $request->currency,
                    // "externalKey"           => $request->externalKey,
                    // "timeZone"              => $request->timeZone
        ];
        // return $endpoint;
        $requestOptions = [
                'auth'  => [$username, $password],
                'headers' => $this->headers,
                'json' => $bodyData,
             ];
        try{
            $client = new Client();
            $response = $client->put($endpoint, $requestOptions);
            $message = "Account updated successfully";
            
            $data['status'] = true;
            $data['message'] = $message;
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
            
        }catch(ClientException $e){
            $response = $e->getResponse();
            $data['status'] = false;
            $content = json_decode($response->getBody()->getContents(), true);
            $data['message'] = ($content["message"])? $content["message"]: "Account updation failed.";
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }
    /**************** BILLING DETAIL ********************/


    /**************** CLOSE ACCOUNT ********************/
    public function destroy($account_id){

        $userCred = getUserFromToken($headers["token"]);
        $this->credential[0] = $username = $userCred[0];
        $this->credential[1] = $password = $userCred[1];

    	$endpoint = env('API_BASE_URL').'/accounts/'.$account_id.'?cancelAllSubscriptions=true&writeOffUnpaidInvoices=true&itemAdjustUnpaidInvoices=true';
    	try{
    		$client = new Client();
        	$response = $client->delete($endpoint, ['auth'=> $this->credential, 'headers'=> $this->headers]);
        	$data['status'] = true;
        	$data['data'] = '';
        	$data['message'] = 'Account closed successfully.';
        	if ($response->getStatusCode() === 200) {
        		return response()->json(['result' => $data], $response->getStatusCode());
        	}
        	
    	}catch(ClientException $e){
    		$response = $e->getResponse();
            $data['status'] = false;
            $content = json_decode($response->getBody()->getContents(), true);
            $data['message'] = ($content["message"])? $content["message"]: "Account closing failed.";
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
    	}
    }


    /**************** GET SINGLE ACCOUNT DETAIL ****************/
    public function getSingleAccountDetail($accountId){
        
        $endpoint                           = env('API_BASE_URL').'/accounts/'.$accountId;

        $headers                            = apache_request_headers();
        if(array_key_exists('token', $headers)){
            $credentials    = getUserFromToken($headers['token']);
            // return $credentials;
            if($credentials){

                try{

                    $client             = new Client();
                    $response           = $client->get($endpoint, ['auth'=> $credentials, 'headers'=> $this->headers]);

                    $data['status']     = true;
                    $data['message']    = 'Account record found successfully.';
                    $data['data']       = json_decode($response->getBody()->getContents(), true);

                    return response()->json(['result' => $data], $response->getStatusCode());
                }catch(ClientException $e){
                    $response = $e->getResponse();
                    // dd((json_decode($response->getBody()->getContents(), true))['message']);

                    $data['status']     = false;
                    $content    = json_decode($response->getBody()->getContents(), true);
                    $data['message']    = (!empty($content["message"])) ? $content["message"] : 'Account not found.';

                    return response()->json(['result' => $data], $response->getStatusCode());
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }
    /**************** GET SINGLE ACCOUNT DETAIL ****************/
}
