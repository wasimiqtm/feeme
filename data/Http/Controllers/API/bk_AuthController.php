<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    private $headers = [
                            'Accept'                => 'application/json',
                            'Content-Type'          => 'application/json',
                            // 'X-Killbill-ApiKey'     => 'bob',
                            // 'X-Killbill-ApiSecret'  => 'lazar',
                       ];
    private $credential   = [];

    public $errorStatus = 401;

    public function login(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $this->credential[] = $username;
        $this->credential[] = $password;

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }


        // return $this->credential;
        try {
            $endpoint = env('API_BASE_URL').'/security/subject';
            $client = new Client();
            $response = $client->get($endpoint, ['auth'=> $this->credential, 'headers'=> $this->headers]);

            if ($response->getStatusCode() >= 200 && $response->getStatusCode() <=299) { 
                // return $response->getBody()->getContents();
                $data['status'] = true;
                $data['message'] = "Credentials matched.";
                $data['data'] = json_decode($response->getBody()->getContents(), true);

                return response()->json(['result' => $data], $response->getStatusCode());
            } 
        }
        catch (ClientException $e) {
            $response = $e->getResponse();
            if (!empty($response) && isset($response)) {
                $data['status'] = false;
                $data['message'] = $response->getReasonPhrase().' user or password.';
                $data['data'] = '';
            }else{
                $data['status'] = false;
                $data['message'] = 'Email or Password does\'nt match.'; 
            }
            return response()->json(['result' => $data], $response->getStatusCode());
            
        }
    }
    public function signup(Request $request){

        $validator = Validator::make($request->all(), [
            'username'      => 'required',
            'surname'       => 'required',
            'email'         => 'required|email',
            'mobile_number' => 'required|numeric',
            'password'      => 'required|string|min:6',

        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $bodyData = ['username' => $request->username, 'password' => $request->password, "roles" => [""]];
        $requestOptions = [
                // 'auth'  => ['admin', 'password'],
                'headers' => [
                            'Accept'                => 'application/json',
                            'Content-Type'          => 'application/json',
                            'X-Killbill-CreatedBy'  => 'Fee Me Web',
                            'X-Killbill-ApiKey'     => 'bob',
                            'X-Killbill-ApiSecret'  => 'lazar',
                            // 'Authorization'  => '41b50106-5fae-487b-8fcf-5e135a8209b9',
                        ],
                'json' => $bodyData,
             ];

         // return $bodyData;
         // return $requestOptions;
        $endpoint = env('API_BASE_URL').'/security/users';

        try {
            $client = new Client();
            $response = $client->post($endpoint, $requestOptions);  
            
            if ($response->getStatusCode() >= 200 && $response->getStatusCode() <=299) { 
                $data['status'] = true;
                $data['message'] = "User created.";
                $data['data'] = '';

                return response()->json(['result' => $data], $response->getStatusCode());
            } 
        }
        catch (ClientException $e) {
            $response = $e->getResponse();
            $raw_data = $response->getBody()->getContents();
            $final_data = json_decode($raw_data, true);

            if (!empty($final_data['message'])) {
                $content = $final_data['message'];
            }else{
                $content = 'User exists.';
            }
            $data['status'] = false;
            $data['message'] = $content;
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
            
        }
    }
}
