<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;

class bk_AccountController extends Controller
{
    private $headers = [
                            'Accept'                => 'application/json',
                            // 'X-Killbill-ApiKey'     => 'bob',
                            // 'X-Killbill-ApiSecret'  => 'lazar',
                       ];
    private $auth   = ['zeshan', '123456'];

    // $baseUrl = env('API_BASE_URL');
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        /************************* FOR DELETE  *****************************/
        // turn on debugging mode.  This will force guzzle to dump the request and response.
        // $client = new GuzzleHttp\Client(['debug' => true,]);

        // this option will also set the 'Content-Type' header.
        /*$response = $client->delete($uri, [
            'json' => $data,
        ]);*/
        /************************* FOR DELETE  *****************************/


        $baseUrl = env('API_BASE_URL');
        /*$body = [
                "username" => "test1",
                "password" => "test2",
                "roles" => ["admin", "user:create"],
            ];

        $endpoint = $baseUrl.'/security/users';
        // return $endpoint;
        $options = [
            // 'http_errors' => false,
            'auth'  => ['admin', 'password'],
            'headers' => [
                            'Accept'                => 'application/json',
                            'Content-Type'          => 'application/json',
                            'X-Killbill-ApiKey'     => env('API_KEY'),
                            'X-Killbill-ApiSecret'  => env('API_SECRET'),
                            'X-Killbill-CreatedBy'  => 'Laravel',
                        ],
            'json' => $body,
             ];


        try {
            $client = new Client();
            $response = $client->post($endpoint, $options);

            if ($response->getStatusCode() === 201) { //201 for created
                $data['status'] = true;
                $data['Message'] = "Account created successfully.";
                $data['data'] = $body;
                return response()->json(['result' => $data], 201);
            } 
        }
        catch (ClientException $e) {
            $response = $e->getResponse();
            // return $response;
            $responseBodyAsString = json_decode($response->getBody());//json_encode($response->getBody()->getContents());
            $data['status'] = false;
            $data['Message'] = $responseBodyAsString->message;
            $data['data'] = '';
            return response()->json(['result' => $data], $response->getStatusCode());
        }*/


        //for put request 
        /*$client = new Client();
        $client->put($endpoint, $options);*/

        
        // dd($response);
        // return response()->json(['result' => 'outside'], 200);
        ///////////////////////////////////////////

        $client = new Client(); //
        $response = $client->request('GET', $baseUrl.'/security/subject', 
                                        ['auth'=> $this->auth, 'headers'=> $this->headers]);
        
        $data = [];
        $data['status_code'] = $response->getStatusCode();
        $data['status']  = false;
        $data['message'] = 'No data found.';
        // $data['data']    = $response->getBody();

        // dd(json_decode($response));

        // return $response;
        if($response->getStatusCode() === 200){
            // return $response->getBody();
            $data['status'] = true;
            $data['message'] = 'success';
            $data['result']    = json_decode($response->getBody());
            // return $data;/
            return response()->json(['result' => $data]);
        }

        // return $response->getStatusCode();
        // return $reason = $response->getReasonPhrase();
        //return $body = $response->getBody();
        
        return response()->json(['result' => $data]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
