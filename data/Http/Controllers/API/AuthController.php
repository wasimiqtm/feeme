<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Validator;
use App\User;

class AuthController extends Controller
{
    private $headers = [
                            'Accept'                => 'application/json',
                            'Content-Type'          => 'application/json',
                            // 'X-Killbill-ApiKey'     => 'bob',
                            // 'X-Killbill-ApiSecret'  => 'lazar',
                       ];
    private $credential   = [];

    public $successStatus   = 200;
    public $errorStatus     = 401;

    public function login(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $this->credential[] = $username;
        $this->credential[] = $password;

        $validator = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }


        // return $this->credential;
        try {
            $endpoint = env('API_BASE_URL').'/security/subject';
            $client = new Client();
            $response = $client->get($endpoint, ['auth'=> $this->credential, 'headers'=> $this->headers]);

            if ($response->getStatusCode() >= 200 && $response->getStatusCode() <=299) { 
                // return $response->getBody()->getContents();
                $data['status'] = true;
                $data['message'] = "Credentials matched.";
                $data['data'] = json_decode($response->getBody()->getContents(), true);

                $encoded_data = base64_encode($request->username.':'.$request->password);
                $token = create_user($encoded_data);

                $data['data']['token'] = $token;

                return response()->json(['result' => $data], $response->getStatusCode());
            } 
        }
        catch (ClientException $e) {
            $response = $e->getResponse();
            if (!empty($response) && isset($response)) {
                $data['status'] = false;
                $data['message'] = $e->getResponse()->getReasonPhrase().' user or password.';
                // $data['data'] = '';
            }else{
                $data['status'] = false;
                $data['message'] = 'Email or Password does\'nt match.'; 
            }
            return response()->json(['result' => $data], $response->getStatusCode());
            
        }
        /*catch(BadResponseException $e){
            return response()->json(['result' => $e->getMessage()], $response->getStatusCode());
        }*/
    }
    public function signup(Request $request){

        $validator = Validator::make($request->all(), [
            'username'      => 'required',
            'surname'       => 'required',
            'email'         => 'required|email',
            'mobile_number' => 'required|numeric',
            'password'      => 'required|string|min:6',

        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $bodyData = ['username' => $request->username, 'password' => $request->password, "roles" => [""]];
        $requestOptions = [
                'auth'  => ['admin', 'password'],
                'headers' => [
                            'Accept'                => 'application/json',
                            'Content-Type'          => 'application/json',
                            'X-Killbill-CreatedBy'  => 'Fee Me Web',
                            'X-Killbill-ApiKey'     => env('API_KEY'),
                            'X-Killbill-ApiSecret'  => env('API_SECRET'),
                        ],
                'json' => $bodyData,
             ];

         // return $bodyData;
         // return $requestOptions;
        $endpoint = env('API_BASE_URL').'/security/users';

        try {
            $client = new Client();
            $response = $client->post($endpoint, $requestOptions);  
            
            if ($response->getStatusCode() >= 200 && $response->getStatusCode() <=299) { 
                $data['status'] = true;
                $data['message'] = "User created.";
                $data['data'] = '';

                return response()->json(['result' => $data], $response->getStatusCode());
            } 
        }
        catch (ClientException $e) {
            $response = $e->getResponse();
            $raw_data = $response->getBody()->getContents();

            $final_data = json_decode($raw_data, true);
            // return $final_data['message'];

            if (!empty($final_data['message'])) {
                // dd($content);
                $content = $final_data['message'];
            }else{
                $content = 'User exists.';
            }
            $data['status'] = false;
            $data['message'] = $content;
            // $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }

    public function logout(Request $request)
    {
        $headers = apache_request_headers();
        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);
            if($checkToken){
                
                $user           = User::where('token' , $token)->firstOrFail();
                $user->delete();
                $data['status'] = true;
                $data['message'] = "User logged out successfully.";

                return response()->json(['result' => $data], $this->successStatus);
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }
}
