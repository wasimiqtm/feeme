<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//FOR GUZZLE
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;

class CatalogController extends Controller
{
    private $headers = [
                            'Accept'                => 'application/json',
                            'Content-Type'          => 'application/json',
                            'X-Killbill-CreatedBy'  => 'Fee Me Web'
                       ];
    private $credential   = [];
    
    public function __construct(){
	    $this->headers['X-Killbill-ApiKey'] 	= env('API_KEY');
	    $this->headers['X-Killbill-ApiSecret'] 	= env('API_SECRET');
	}
    public function getBasePlans(){
        $headers = apache_request_headers();
        $userCred = getUserFromToken($headers['token']);
        $this->credential[0] = $userCred[0];
        $this->credential[1] = $userCred[1];
        
        $endpoint = env('API_BASE_URL').'/catalog/availableBasePlans'; 
        try{
            $client = new Client();
            $response = $client->get($endpoint, ['auth'=> $this->credential, 'headers'=> $this->headers]);
            $data['data'] = json_decode($response->getBody()->getContents(), true);

            $data['status']     = true;
            $data['message']    = 'Plans found successfully.';

            return response()->json(['result' => $data], $response->getStatusCode());
        }catch(ClientException $e){
            $response = $e->getResponse();
            $data['status'] = false;
            $data['data'] = '';

            $content = json_decode($response->getBody()->getContents(), true);
            $data['message'] = (!empty($content["message"])) ? $content["message"] : 'Plans not found.';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }
    public function getProducts(){
    	$headers = apache_request_headers();
        $userCred = getUserFromToken($headers['token']);
        $this->credential[0] = $userCred[0];
        $this->credential[1] = $userCred[1];
        
    	$endpoint = env('API_BASE_URL').'/catalog/'; 
    	try{
    		$client = new Client();
        	$response = $client->get($endpoint, ['auth'=> $this->credential, 'headers'=> $this->headers]);

            /***************** FETCHED DATA *******************/
        	$data['data'] = json_decode($response->getBody()->getContents(), true);
        	$data['status']		= true;
        	$data['message'] 	= 'Products found successfully.';

        	return response()->json(['result' => $data], $response->getStatusCode());
    	}catch(ClientException $e){
    		$response = $e->getResponse();
            $data['status'] = false;
            $data['data'] = '';

            $content = json_decode($response->getBody()->getContents(), true);
            $data['message'] = (!empty($content["message"])) ? $content["message"] : 'Products not found.';

            return response()->json(['result' => $data], $response->getStatusCode());
    	}
    }
    public function addProduct(Request $request){
        $userCred = getUserFromToken($request->header('token'));
        $request = $request;
        // return $request->all();
        // return $userCred;
        $bodyData = [
                    "planId"            => $request["planName"],
                    "productName"       => $request["productName"],
                    "productCategory"   => $request["category"],
                    "currency"          => $request["currency"],
                    "amount"            => $request["amount"],
                    "billingPeriod"     => $request["billingPeriod"],
                    "trialLength"       => 0,
                    "trialTimeUnit"     => "DAYS"
        ];
        // return $bodyData;
        $requestOptions = [
                'auth'  => $userCred,
                'headers' => $this->headers,
                'json' => $bodyData,
             ];
        // return $requestOptions;
        $endpoint = env('API_BASE_URL').'/catalog/simplePlan';
        try{
            $client = new Client();
            $response = $client->post($endpoint, $requestOptions);  

            $message = "Product and plan created successfully";
            if ((int) $response->getStatusCode() > 199 && (int) $response->getStatusCode() < 300) {
                $data['status'] = true;
                $data['message'] = $message;
                $data['data'] = $bodyData;

                return response()->json(['result' => $data], $response->getStatusCode());                 
            } 
            
        }catch(ClientException $e){
            $response = $e->getResponse();
            $data['status'] = false;
            $content = json_decode($response->getBody()->getContents(), true);
            $data['message'] = ($content["message"])? $content["message"]: "Product and plan creation failed.";
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }
}
