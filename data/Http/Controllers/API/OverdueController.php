<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//FOR GUZZLE
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;

class OverdueController extends Controller
{
    private $headers = [
                            'Accept'                => 'application/json',
                            'Content-Type'          => 'application/json',
                            'X-Killbill-CreatedBy'  => 'Fee Me Web'
                       ];
    private $credential   = [];
    
    public function __construct(){
	    $this->headers['X-Killbill-ApiKey'] 	= env('API_KEY');
	    $this->headers['X-Killbill-ApiSecret'] 	= env('API_SECRET');
	}
    public function getAccountOverdue($accountId){


        $headers = apache_request_headers();
        $userCred = getUserFromToken($headers['token']);
        $this->credential[0] = $userCred[0];
        $this->credential[1] = $userCred[1];
    	$endpoint = env('API_BASE_URL').'/accounts/'.$accountId.'/overdue';  
    	
    	try{

    		$client = new Client();
        	$response = $client->get($endpoint, ['auth'=> $this->credential, 'headers'=> $this->headers]);
        	$data['data'] = json_decode($response->getBody()->getContents(), true);

        	$data['status'] 		= true;
        	$data['message'] 		= 'Overdue found successfully.';

        	return response()->json(['result' => $data], $response->getStatusCode());
    	}catch(ClientException $e){
    		$response = $e->getResponse();
    		// dd($response->getBody()->getContents());
            $data['status'] = false;
            $data['data'] = '';

            switch ($response->getStatusCode()) {
            	case '400':
            		$data['message'] = 'Invalid account id supplied.';
            	break;
            	case '404':
            		$data['message'] = 'Account not found.';
            	break;
            	
            	default:
            		$content = json_decode($response->getBody()->getContents(), true);
                    $data['message'] = (!empty($content["message"])) ? $content["message"] : 'Overdue not found.';
            	break;
            }

            return response()->json(['result' => $data], $response->getStatusCode());
    	}
    }
    public function setHeaders($headers){
    	/*$this->credential[0] = $headers['username'];
		$this->credential[1] = $headers['password'];*/

    	$this->credential[0] = base64_decode($headers['username']);
		$this->credential[1] = base64_decode($headers['password']);
    }
}
