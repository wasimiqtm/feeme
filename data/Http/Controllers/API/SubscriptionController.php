<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//FOR GUZZLE
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;

class SubscriptionController extends Controller
{
	private $headers = [
                            'Accept'                => 'application/json',
                            'Content-Type'          => 'application/json',
                            'X-Killbill-CreatedBy'  => 'Fee Me Web',
                       ];
    private $credential   = [];//'admin', 'password'
    public function __construct(){
        $this->headers['X-Killbill-ApiKey']     = env('API_KEY');
        $this->headers['X-Killbill-ApiSecret']  = env('API_SECRET');
    }
    public function index(){
    	return response()->json('success', 200);
    }
    public function getAccountBundle($accountId){
    	// return $accountId;
    	$endpoint = env('API_BASE_URL').'/accounts/'.$accountId.'/bundles';    	
    	$headers = apache_request_headers();
        $userCred = getUserFromToken($headers['token']);
        $this->credential[0] = $userCred[0];
        $this->credential[1] = $userCred[1];
        // return $this->headers;
    	try{

    		$client = new Client();
        	$response = $client->get($endpoint, ['auth'=> $this->credential, 'headers'=> $this->headers]);
        	$data['data'] = json_decode($response->getBody()->getContents(), true);

        	$data['status'] 		= true;
        	$data['message'] 		= 'Bundle found successfully.';

        	return response()->json(['result' => $data], $response->getStatusCode());
    	}catch(ClientException $e){
    		$response = $e->getResponse();
    		// dd($response->getBody()->getContents());
            $data['status'] = false;
            $data['data'] = '';

            switch ($response->getStatusCode()) {
            	case '400':
            		$data['message'] = 'Invalid account id supplied.';
            	break;
            	case '404':
            		$data['message'] = 'Account not found.';
            	break;
            	
            	default:
            		$content = json_decode($response->getBody()->getContents(), true);
                    $data['message'] = (!empty($content["message"])) ? $content["message"] : 'Bundle not found.';
            	break;
            }

            return response()->json(['result' => $data], $response->getStatusCode());
    	}
    }
    public function addSubscription(Request $request){
        
        $headerArray = setHeaders($request->header());
        $this->credential[0] = $username = $headerArray[0];
        $this->credential[1] = $password = $headerArray[1];

        // return date('Y-m-d', time());
        $todayDate = date('Y-m-d', time());
        $entitlementDate = (!empty($request->entitlementDate) ? date('Y-m-d', strtotime($request->entitlementDate)) :  $todayDate);
        $bodyData = [
                    "accountId"         => $request->accountId,
                    "externalKey"       => (!empty($request->externalKey) ? $request->externalKey : ''),
                    "planName"          => $request->plan
        ];
        if (!empty($entitlementDate)) {
            $bodyData["billingStartDate"]   = $entitlementDate;
            $bodyData["chargedThroughDate"] = $entitlementDate;
            $bodyData["startDate"]          = $entitlementDate;
        }
        // return $bodyData;
        $requestOptions = [
                'auth'  => $this->credential,
                'headers' => $this->headers,
                'json' => $bodyData,
             ];

        $endpoint = env('API_BASE_URL').'/subscriptions?entitlementDate='.$entitlementDate;
        try{
            $client = new Client();
            $response = $client->post($endpoint, $requestOptions);  
            //POST /1.0/kb/subscriptions
            $message = "Subscription created successfully";
            if ($response->getStatusCode() >= 200 || $response->getStatusCode() <= 299) { 
                $data['status'] = true;
                $data['message'] = $message;
                $data['data'] = '';

                return response()->json(['result' => $data], $response->getStatusCode());
            } 
            
        }catch(ClientException $e){
            $response = $e->getResponse();
            // dd($response);
            $data['status'] = false;
            $data['data'] = '';
            $content = json_decode($response->getBody()->getContents(), true);
            // return $response->getBody()->getContents();
            $data['message'] = (!empty($content["message"])) ? $content["message"] : 'Subscription failed to create.';
            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }
}
