<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsCallTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_call', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
            $table->string('account_id');
            $table->bigInteger('number');
            $table->integer('incoming');
            $table->integer('duration');
            $table->integer('count');
            $table->dateTime('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_call');
    }
}
