<?php

return [
    'feeme_unit' => [
        'emails' 		=> 'count',
        'email' 		=> 'count',
        'sms' 			=> 'sms-count',
        'call' 			=> 'call-per-minute',
        'meeting' 		=> 'per-minute',
        'travel' 		=> 'per-minute',
    ]
];

?>