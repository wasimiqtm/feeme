<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subscriptions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type', 'location', 'notes', 'start_time', 'end_time', 'account_id', 'subscription_id', 'number', 'incoming','duration', 'count', 'date', 'email_message_id'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $casts = [
        'location' => 'array',
    ];

    /*use SoftDeletes;
    protected $dates = ['deleted_at'];*/
}
