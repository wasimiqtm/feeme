<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//FOR GUZZLE
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\DB;

use App\Subscription;

class UsageController extends Controller
{
	public $successStatus   = 200;
    public $errorStatus     = 401;

    private $headers = [];

    public function __construct(){

        $this->headers['Accept']                = 'application/json';
        $this->headers['Content-Type']          = 'application/json';
        $this->headers['X-Killbill-ApiKey']     = env('API_KEY');
        $this->headers['X-Killbill-ApiSecret']  = env('API_SECRET');
        $this->headers['X-Killbill-CreatedBy']  = 'Fee Me Web';
    }

    public function index(){
    	return response()->json('success', 200);
    }

    public function getUsageAgainstSubscription($subscriptionId, $startDate, $endDate, $param = false){


        /*$startDate = $request->input('startDate');
        $endDate = $request->input('endDate');*/
    	
    	$endpoint = env('API_BASE_URL').'/usages/'.$subscriptionId.'?startDate='.$startDate.'&endDate='.$endDate;   

        $headers = apache_request_headers();
        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);

            if($checkToken){

                try{
                    $client = new Client();
                    $response = $client->get($endpoint, ['auth'=> $checkToken, 'headers'=> $this->headers]);
                    $data['status']         = true;
                    $data['message']        = 'Usage found successfully.';

                    $result = json_decode($response->getBody()->getContents(), true);
                    // change date format
                    $result['startDate']    = date('d M Y h:i:s A', strtotime($result['startDate']));
                    $result['endDate']      = date('d M Y h:i:s A', strtotime($result['endDate']));

                    $data['data'] = $result;

                    if($param){
                        return $data['data'];
                    }else{
                        return response()->json(['result' => $data], $response->getStatusCode());
                    }
                    
                }catch(ClientException $e){
                    $response = $e->getResponse();
                    // dd($response->getBody()->getContents());
                    $data['status'] = false;
                    // $data['data'] = '';

                    switch ($response->getStatusCode()) {
                        case '400':
                            $data['message'] = 'Invalid subscription id supplied.';
                        break;
                        case '404':
                            $data['message'] = 'subscription not found.';
                        break;
                        
                        default:
                            $data['message'] = (!empty(json_decode($response->getBody()->getContents())->message)) ? json_decode($response->getBody()->getContents(), true)->message : 'Bundle not found.';
                        break;
                    }

                    if($param){
                        return $data['data'] = '';
                    }else{
                        return response()->json(['result' => $data], $response->getStatusCode());
                    }
                }

            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    public function getUsageAgainstSubscriptionAndUnitType(Request $request, $subscriptionId, $unitType){

        $startDate = $request->input('startDate');
        $endDate = $request->input('endDate');
        
        $endpoint = env('API_BASE_URL').'/usages/'.$subscriptionId.'/'.$unitType.'?startDate='.$startDate.'&endDate='.$endDate;  

        $headers = apache_request_headers();
        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);

            if($checkToken){

                try{
                    $client = new Client();
                    $response = $client->get($endpoint, ['auth'=> $checkToken, 'headers'=> $this->headers]);
                    $data['status']         = true;
                    $data['message']        = 'Usage found successfully.';

                    $data['data'] = json_decode($response->getBody()->getContents(), true);

                    return response()->json(['result' => $data], $response->getStatusCode());
                }catch(ClientException $e){
                    $response = $e->getResponse();
                    // dd($response->getBody()->getContents());
                    $data['status'] = false;
                    // $data['data'] = '';

                    switch ($response->getStatusCode()) {
                        case '400':
                            $data['message'] = 'Invalid subscription id supplied.';
                        break;
                        case '404':
                            $data['message'] = 'subscription not found.';
                        break;
                        
                        default:
                            $data['message'] = (!empty(json_decode($response->getBody()->getContents())->message)) ? json_decode($response->getBody()->getContents(), true)->message : 'Bundle not found.';
                        break;
                    }

                    return response()->json(['result' => $data], $response->getStatusCode());
                }

            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    /*public function addUsage(Request $request){

        $json_array = $request->input_json_object;
        $input = json_decode($json_array, true);

        $headers = apache_request_headers();
        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);
            if($checkToken){

                $dataArray = array();
                foreach($input as $row){
                    
                    $date = date('Y-m-d', strtotime($row['date']));

                    if($row['type'] == 'call'){

                        $amount_in_sec = $row['duration'];
                        $amount     = ceil($amount_in_sec/60);
                        $row['duration'] = $amount;
                        $unitType   = 'call-per-minute';

                    }elseif($row['type'] == 'sms'){

                        $amount = $row['count'];
                        $unitType = 'sms-count';

                    }elseif($row['type'] == 'meeting'){

                        //duplicate meeting check
                        if((isset($row['account_id']) && $row['account_id'] != '')){
                            $meetingData = Subscription::Where(['account_id' => $row['account_id'], 'start_time' => $row['start_time'], 'end_time' => $row['end_time']])->first();
                        
                            if($meetingData){
                                $data['status'] = false;
                                $data['message'] = "Meeting already added against requested time intervals.";

                                return response()->json(['result' => $data], $this->errorStatus);
                            }
                        }

                        $amount_in_sec = $row['duration'];
                        $amount     = ceil($amount_in_sec/60);
                        $row['duration'] = $amount;
                        $unitType = 'per-minute';

                    }else{

                        $amount = 1;
                        $unitType = 'string';
                    }
                    
                    // data array send to server
                    if((isset($row['account_id']) && $row['account_id'] != '') && (isset($row['subscription_id']) && $row['subscription_id'] != '')){
                        
                        $dataArray[] = array(
                            'subscriptionId'   => $row['subscription_id'],
                            'unitUsageRecords' => array(array(
                                'unitType'          => $unitType,
                                'usageRecords'      => array(array(
                                    'recordDate'        => $date,
                                    'amount'            => $amount
                                ))  
                            ))
                        );
                    }
                    // save data locally
                    Subscription::Create($row);
                }
                // return $dataArray;

                // send data to server
                if(count($dataArray) > 0){
                    
                    foreach($dataArray as $rowArray){

                        $credentials    = getUserFromToken($headers['token']);
                        if($credentials){

                            $bodyData = $rowArray;
                            // return $bodyData;
                            $requestOptions = [
                                'auth'  => $credentials,
                                'headers' => $this->headers,
                                'json' => $bodyData,
                            ];
                            $endpoint = env('API_BASE_URL').'/usages';
                            // return $requestOptions;

                            try{
                                $client = new Client();
                                $response = $client->post($endpoint, $requestOptions);  

                            }catch(ClientException $e){
                                $response = $e->getResponse();
                                $data['status'] = false;
                                $content = json_decode($response->getBody()->getContents(), true);
                                $data['message'] = (!empty($content["message"])) ? $content["message"] : 'usage not added.';
                                
                                return response()->json(['result' => $data], $response->getStatusCode());
                            }
                        }
                    }
                    $data['status'] = true;
                    $data['message'] = "Usage added successfully.";
                    $data['data'] = $dataArray;
                }else{
                    $data['status'] = true;
                    $data['message'] = "Usage added on local server successfully.";
                    $data['data'] = $input;
                }
                return response()->json(['result' => $data], $this->successStatus);

            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }*/

    public function addUsage(Request $request){

        $json_array = $request->input_json_object;
        $input = json_decode($json_array, true);


        $headers = apache_request_headers();
        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);
            if($checkToken){

                $dataArray = array();
                foreach($input as $row){
                    
                    $date = date('Y-m-d', strtotime($row['date']));

                    if($row['type'] == 'call'){

                        $amount_in_sec = $row['duration'];
                        $amount     = ceil($amount_in_sec/60);
                        $row['duration'] = $amount;
                        $unitType   = 'call-per-minute';

                    }elseif($row['type'] == 'sms'){

                        $amount = $row['count'];
                        $unitType = 'sms-count';

                    }elseif($row['type'] == 'meeting'){

                        //duplicate meeting check
                        if((isset($row['account_id']) && $row['account_id'] != '')){
                            $meetingData = Subscription::Where(['account_id' => $row['account_id'], 'start_time' => $row['start_time'], 'end_time' => $row['end_time']])->first();
                        
                            if($meetingData){
                                $data['status'] = false;
                                $data['message'] = "Meeting already added against requested time intervals.";

                                return response()->json(['result' => $data], $this->errorStatus);
                            }
                        }

                        $amount_in_sec = $row['duration'];
                        $amount     = ceil($amount_in_sec/60);
                        $row['duration'] = $amount;
                        $unitType = 'per-minute';

                    }elseif($row['type'] == 'travel'){

                        $amount_in_sec = $row['duration'];
                        $amount     = ceil($amount_in_sec/60);
                        $row['duration'] = $amount;
                        $unitType = 'per-minute';

                    }else{

                        $amount = 1;
                        $unitType = 'string';
                    }
                    
                    // data array send to server
                    if((isset($row['account_id']) && $row['account_id'] != '') && (isset($row['subscription_id']) && $row['subscription_id'] != '')){
                        
                        $dataArray[] = array(
                            'subscriptionId'   => $row['subscription_id'],
                            'unitUsageRecords' => array(array(
                                'unitType'          => $unitType,
                                'usageRecords'      => array(array(
                                    'recordDate'        => $date,
                                    'amount'            => $amount
                                ))  
                            ))
                        );
                    }
                    // save data locally
                    Subscription::Create($row);
                }
                // return $dataArray;

                // send data to server
                if(count($dataArray) > 0){
                    
                    foreach($dataArray as $rowArray){

                        $credentials    = getUserFromToken($headers['token']);
                        if($credentials){

                            $bodyData = $rowArray;
                            // return $bodyData;
                            $requestOptions = [
                                'auth'  => $credentials,
                                'headers' => $this->headers,
                                'json' => $bodyData,
                            ];
                            $endpoint = env('API_BASE_URL').'/usages';
                            // return $requestOptions;

                            try{
                                $client = new Client();
                                $response = $client->post($endpoint, $requestOptions);  

                            }catch(ClientException $e){
                                $response = $e->getResponse();
                                $data['status'] = false;
                                $content = json_decode($response->getBody()->getContents(), true);
                                $data['message'] = (!empty($content["message"])) ? $content["message"] : 'usage not added.';
                                
                                return response()->json(['result' => $data], $response->getStatusCode());
                            }
                        }
                    }
                    $data['status'] = true;
                    $data['message'] = "Usage added successfully.";
                    $data['data'] = $dataArray;
                }else{
                    $data['status'] = true;
                    $data['message'] = "Usage added on local server successfully.";
                    $data['data'] = $input;
                }
                return response()->json(['result' => $data], $this->successStatus);

            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    public function unAuthenticAttendances(Request $request){

        $account_id = $request->input('account_id');
        $subscription_id = $request->input('subscription_id');
        $type = strtoupper($request->input('type'));

        $request->input('records_per_page');

        $records_per_page = ($request->input('records_per_page'))?:10;

        if(!empty($account_id) && empty($subscription_id)){
            $records = Subscription::where('account_id', $account_id)
            
            ->orderBy('date', 'desc')->paginate($records_per_page);
        }else if(!empty($account_id) && !empty($subscription_id)){
            
            switch ($type) {
                        case 'CALL':

                            $records = Subscription::where('account_id' , $account_id)
                            ->where('subscription_id' , $subscription_id)
                            ->where('type', $type)
                            
                            ->orderBy('date', 'desc')->paginate($records_per_page);
                            break;

                        case 'SMS':
                            $records = Subscription::where('account_id' , $account_id)
                            ->where('subscription_id' , $subscription_id)
                            ->where('type', $type)
                            
                            ->orderBy('date', 'desc')->paginate($records_per_page);
                            break;

                        case 'MEETING':
                            
                            $records = Subscription::where('account_id' , $account_id)
                            ->where('subscription_id' , $subscription_id)
                            ->where('type', $type)
                            
                            ->orderBy('date', 'desc')->paginate($records_per_page);
                            break;

                        case 'TRAVEL':
                            $records = Subscription::where('account_id' , $account_id)
                            ->where('subscription_id' , $subscription_id)
                            ->where('type', $type)
                            
                            ->orderBy('date', 'desc')->paginate($records_per_page);
                            break;

                        case 'TRAVEL-TIME':
                            $records = Subscription::where('account_id' , $account_id)
                            ->where('subscription_id' , $subscription_id)
                            ->where('type', 'travel')
                            
                            ->orderBy('date', 'desc')->paginate($records_per_page);
                            break;
                        
                        default:
                            
                            $records = Subscription::where('account_id' , $account_id)
                            ->where('subscription_id' , $subscription_id)
                            
                            ->orderBy('date', 'desc')->paginate($records_per_page);
                            break;
                    }
            
        }else{
            $records = Subscription::where('account_id', '')
                    ->where('subscription_id', '')
                    
                    ->orderBy('date', 'desc')->paginate($records_per_page);
        }

        // change date format
        foreach($records as $row){
            $row['start_time']  = date('d M Y h:i:s A', strtotime($row['start_time']));
            $row['end_time']    = date('d M Y h:i:s A', strtotime($row['end_time']));
            $row['date']        = date('d M Y h:i:s A', strtotime($row['date']));
        }

        $data['status'] = true;
        $data['data']   = $records;
        return response()->json(['result' => $data], $this->successStatus);
    }

    public function updateUnAllocated($id, Request $request){
        // return $id;
        // return $request->all();
        /*try{

        }catch(Illuminate\Database\Eloquent\ModelNotFoundException $e){
            return $data['msg'] = 'asd';
        }*/
        
        $subscription = Subscription::findOrFail($id);
        
        switch ($request->type) {
            case 'call':
            case 'travel':
            case 'travel-time':
            case 'meeting':
                $subscription->duration = $request->duration;
            break;

            case 'email':
            case 'emails':
            case 'sms':
                $subscription->count = $request->count;
            break;
        }

        try {
            $subscription->save();
            $data['status'] = true;
            $data['message'] = strtoupper($request->type) . ' updated successfully.';
            $statusCode = 200;
        } catch (\Illuminate\Database\QueryException $e) {
            // something went wrong with the transaction, rollback
            $data['status'] = false;
            $data['message'] = strtoupper($request->type) . ' not updated.';
            $statusCode = 400;
        }
        
        
        return response()->json(['result' => $data], $statusCode);
    }
}
