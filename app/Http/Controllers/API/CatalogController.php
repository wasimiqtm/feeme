<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

//FOR GUZZLE
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;


class CatalogController extends Controller
{
    public $successStatus   = 200;
    public $errorStatus     = 401;

    private $headers = [
                            'Accept'                => 'application/json',
                            'Content-Type'          => 'application/json',
                            'X-Killbill-CreatedBy'  => 'Fee Me Web'
                       ];
    private $credential   = [];
    
    public function __construct(){
        $this->headers['X-Killbill-ApiKey']     = env('API_KEY');
        $this->headers['X-Killbill-ApiSecret']  = env('API_SECRET');
    }
    /** GET ALL CATALOGS **/
    public function index(){

        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $credentials    = getUserFromToken($headers['token']);
            if($credentials){
                $endpoint      = env('API_BASE_URL').'/catalog';
                try{
                    $client    = new Client();
                    $response  = $client->get($endpoint, ['auth'=> $credentials, 'headers'=> $this->headers]);
                    
                    $data['data'] = json_decode($response->getBody()->getContents(), true);
                    $data['status']     = true;
                    $data['message']    = 'Products found successfully.';

                    return response()->json(['result' => $data], $response->getStatusCode());
                }catch(ClientException $e){

                    $response           = $e->getResponse();
                    $content            = json_decode($response->getBody()->getContents(), true);
                    $data['message']    = (!empty($content["message"])) ? $content["message"] : 'Products not found.';

                    return response()->json(['result' => $data], $response->getStatusCode());
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }
    public function getBasePlans(Request $request){

        $accountId = $request->input('accountId');
        $accountsubscriptions = app('App\Http\Controllers\API\SubscriptionController')->getAccountBundle($accountId,true);

        $added_data = array();
        if($accountsubscriptions){

            $subscriptions_data = array_column($accountsubscriptions, 'subscriptions');

            foreach($subscriptions_data as $subscription_row){
                $subscription_plan_name = $subscription_row[0]['planName'];
                $added_data[$subscription_plan_name] = array(
                                                                'id'   =>  $subscription_row[0]['subscriptionId'], 
                                                                'status'   =>  $subscription_row[0]['state']
                                                            );
            }
        }


        $headers = apache_request_headers();
        $userCred = getUserFromToken($headers['token']);
        $this->credential[0] = $userCred[0];
        $this->credential[1] = $userCred[1];
        
        $endpoint = env('API_BASE_URL').'/catalog/availableBasePlans'; 
        try{
            $client = new Client();
            $response = $client->get($endpoint, ['auth'=> $this->credential, 'headers'=> $this->headers]);
            $data['data'] = json_decode($response->getBody()->getContents(), true);
            
            $f_array['data'] = array();
            foreach($data['data'] as $row){
               
                if( isset($added_data[$row['plan']]) ) {
                    
                    $row['subscriptionId'] = $added_data[$row['plan']]['id'];
                    $row['status'] = $added_data[$row['plan']]['status'];
                    $f_array['data'][] = $row;
                } else {

                    $row['subscriptionId'] = '';
                    $row['status'] = '';
                    $f_array['data'][] = $row;  
                }
            }
            
            $f_array['status']     = true;
            $f_array['message']    = 'Plans found successfully.';

            return response()->json(['result' => $f_array], $response->getStatusCode());
        }catch(ClientException $e){
            $response = $e->getResponse();
            $data['status'] = false;
            $data['data'] = '';

            $content = json_decode($response->getBody()->getContents(), true);
            $data['message'] = (!empty($content["message"])) ? $content["message"] : 'Plans not found.';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }

    public function getProduct($subscriptionId, $param = false){
        
        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $credentials    = getUserFromToken($headers['token']);
            if($credentials){
                $endpoint      = env('API_BASE_URL').'/catalog/product?subscriptionId='.$subscriptionId;
                try{
                    
                    $client    = new Client();
                    $response  = $client->get($endpoint, ['auth'=> $credentials, 'headers'=> $this->headers]);
                    
                    $data['status']     = true;
                    $data['message']    = 'Products found successfully.';
                    $data['data']       = json_decode($response->getBody()->getContents(), true);

                    if($param){
                        return $data['data'];
                    }else{
                        return response()->json(['result' => $data], $response->getStatusCode());
                    }

                }catch(ClientException $e){

                    $response           = $e->getResponse();
                    $data['status']     = false;
                    $content = json_decode($response->getBody()->getContents(), true);
                    $data['message'] = (!empty($content["message"])) ? $content["message"] : 'Products not found.';
                    if($param){
                        return $data['data'] = '';
                    }else{
                        return response()->json(['result' => $data], $response->getStatusCode());
                    }
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }


    public function addProduct(Request $request){
        $userCred = getUserFromToken($request->header('token'));
        $request = $request;
        // return $request->all();
        // return $userCred;
        $bodyData = [
                    "planId"            => $request["planName"],
                    "productName"       => $request["productName"],
                    "productCategory"   => $request["category"],
                    "currency"          => $request["currency"],
                    "amount"            => $request["amount"],
                    "billingPeriod"     => $request["billingPeriod"],
                    "trialLength"       => 0,
                    "trialTimeUnit"     => "DAYS"
        ];
        // return $bodyData;
        $requestOptions = [
                'auth'  => $userCred,
                'headers' => $this->headers,
                'json' => $bodyData,
             ];
        // return $requestOptions;
        $endpoint = env('API_BASE_URL').'/catalog/simplePlan';
        try{
            $client = new Client();
            $response = $client->post($endpoint, $requestOptions);  

            $message = "Product and plan created successfully";
            if ((int) $response->getStatusCode() > 199 && (int) $response->getStatusCode() < 300) {
                $data['status'] = true;
                $data['message'] = $message;
                $data['data'] = $bodyData;

                return response()->json(['result' => $data], $response->getStatusCode());                 
            } 
            
        }catch(ClientException $e){
            $response = $e->getResponse();
            $data['status'] = false;
            $content = json_decode($response->getBody()->getContents(), true);
            $data['message'] = ($content["message"])? $content["message"]: "Product and plan creation failed.";
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }
}
