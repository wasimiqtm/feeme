<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

//FOR GUZZLE
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;

class InvoiceController extends Controller
{
	public $successStatus   = 200;
    public $errorStatus     = 401;

    private $headers        = [];

    public function __construct(){

        $this->headers['Accept']                = 'application/json';
        $this->headers['Content-Type']          = 'application/json';
        $this->headers['X-Killbill-ApiKey']     = env('API_KEY');
        $this->headers['X-Killbill-ApiSecret']  = env('API_SECRET');
        $this->headers['X-Killbill-CreatedBy']  = 'Fee Me Web';
    }    
    public function index(){
    	return response()->json('success', 200);
    }
    
    /**
    GET ACCOUNT ALL INVOICES
    **/
    public function getInvoices($accountId){

        $endpoint = env('API_BASE_URL').'/accounts/'.$accountId.'/invoices?withItems=false&withMigrationInvoices=false&unpaidInvoicesOnly=false&audit=NONE';        
        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);

            if($checkToken){
                try{
                    $client = new Client();
                    $response = $client->get($endpoint, ['auth'=> $checkToken, 'headers'=> $this->headers]);
                    $data['status']         = true;
                    $data['message']        = 'Invoices found successfully.';

                    $data['data'] = json_decode($response->getBody()->getContents(), true);

                    return response()->json(['result' => $data], $response->getStatusCode());
                }catch(ClientException $e){
                    
                    $response = $e->getResponse();
                    $data['status'] = false;
                    // $data['data'] = '';

                    switch ($response->getStatusCode()) {
                        case '400':
                            $data['message'] = 'Invalid account id supplied.';
                        break;
                        case '404':
                            $data['message'] = 'Account not found.';
                        break;
                        
                        default:
                            $data['message'] = (!empty(json_decode($response->getBody()->getContents())->message)) ? json_decode($response->getBody()->getContents(), true)->message : 'Invoice not found.';
                        break;
                    }
                    return response()->json(['result' => $data], $response->getStatusCode());
                }

            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    /**
    GET ACCOUNT INVOICE PAYMENT
    **/
    public function getAccountInvoicePayment($accountId){

        $endpoint = env('API_BASE_URL').'/accounts/'.$accountId.'/invoicePayments';
        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);

            if($checkToken){
                try{
                    $client = new Client();
                    $response = $client->get($endpoint, ['auth'=> $checkToken, 'headers'=> $this->headers]);
                    $data['status']         = true;
                    $data['message']        = 'Invoice detail found successfully.';

                    $data['data'] = json_decode($response->getBody()->getContents(), true);

                    return response()->json(['result' => $data], $response->getStatusCode());
                }catch(ClientException $e){
                    $response = $e->getResponse();
                    $data['status'] = false;
                    // $data['data'] = '';

                    switch ($response->getStatusCode()) {
                        case '400':
                            $data['message'] = 'Invalid account id supplied.';
                        break;
                        case '404':
                            $data['message'] = 'Account not found.';
                        break;
                        
                        default:
                            $data['message'] = (!empty(json_decode($response->getBody()->getContents())->message)) ? json_decode($response->getBody()->getContents(), true)->message : 'Invoice not found.';
                        break;
                    }

                    return response()->json(['result' => $data], $response->getStatusCode());
                }

            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }


    public function addCharge(Request $request){
        $headers = apache_request_headers();
        if(!array_key_exists('token', $headers)){
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }

        $validator = Validator::make($request->all(), [
            'accountId'     => 'required',
            'notes'         => 'required',
            'amount'        => 'required|integer',
            'currency'      => 'required|string',
            'autoCommit'      => 'required|boolean'
        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $userCred = getUserFromToken($request->header('token'));
        $bodyData = [
                    "description"   => $request->notes,
                    "amount"        => $request->amount,
                    "currency"      => $request->currency
                    ];
        // return $bodyData;
        $requestOptions = [
                'auth'      => $userCred,
                'headers'   => $this->headers,
                'json'      => [$bodyData],
             ];
        // return $requestOptions;
        $autoCommit = $request->autoCommit;
        $accountId = $request->accountId;
        $endpoint = env('API_BASE_URL')."/invoices/charges/$accountId?autoCommit=$autoCommit";
        try{
            $client = new Client();
            $response = $client->post($endpoint, $requestOptions);  

            $message = "Account charged successfully";
            if ((int) $response->getStatusCode() > 199 && (int) $response->getStatusCode() < 300) {
                $data['status'] = true;
                $data['message'] = $message;
                $data['data'] = $bodyData;

                return response()->json(['result' => $data], $response->getStatusCode());                 
            } 
            
        }catch(ClientException $e){
            $response = $e->getResponse();
            $data['status'] = false;
            $content = json_decode($response->getBody()->getContents(), true);
            $data['message'] = ($content["message"])? $content["message"]: "Account not charged.";
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }

    public function getInvoiceDetails($invoiceId){

        $endpoint = env('API_BASE_URL').'/invoices/'.$invoiceId.'?withItems=true&withChildrenItems=false&audit=NONE';   
        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);

            if($checkToken){
                try{
                    $client = new Client();
                    $response = $client->get($endpoint, ['auth'=> $checkToken, 'headers'=> $this->headers]);
                    $data['status']         = true;
                    $data['message']        = 'Invoice detail found successfully.';

                    // $data['data'] = json_decode($response->getBody()->getContents(), true);
                    $invoice_details    = json_decode($response->getBody()->getContents(), true);
                    // return $invoice_details["items"]["1"]["itemDetails"];
                    if(count($invoice_details['items']) > 0){
                        
                        $start_date = date("Y-m-d", strtotime("-15 days", strtotime(date('Y-m-d'))));
                        $end_date   = date("Y-m-d", strtotime("+15 days", strtotime(date('Y-m-d'))));

                        $usage = array();
                        
                        for($i=0; $i < count($invoice_details['items']); $i++) { 
                            // return $invoice_details['items'][$i]["itemDetails"];
                            $sub_id = $invoice_details['items'][$i]['subscriptionId'];
                            $invoice_details['items'][$i]["itemDetails"] = (json_decode($invoice_details['items'][$i]["itemDetails"]))?json_decode($invoice_details['items'][$i]["itemDetails"]) : $invoice_details['items'][$i]["itemDetails"];
                            $get_item_usage         = app('App\Http\Controllers\API\UsageController')->getUsageAgainstSubscription($sub_id, $start_date, $end_date, true);
                            $get_product_details    = app('App\Http\Controllers\API\CatalogController')->getProduct($sub_id, true);
                            
                            /*if($invoice_details['items'][$i]['itemType'] == 'USAGE'){

                                $invoice_details['items'][$i]['productName'] = $get_product_details['name'];
                                $invoice_details['items'][$i]['productType'] = $get_product_details['type'];
                                $invoice_details['items'][$i]['usage'] = $get_item_usage;
                            }*/
                        }
                        // return $invoice_details;
                    }

                    $account_id         = $invoice_details['accountId'];
                    $accountDetails = app('App\Http\Controllers\API\AccountController')->getSingleAccountDetail($account_id,true);

                     $data['data'] = $invoice_details;
                     $data['data']['accountDetails'] = $accountDetails;

                     // return $invoice_details;
                    return response()->json(['result' => $data], $response->getStatusCode());
                }catch(ClientException $e){
                    
                    $response = $e->getResponse();
                    $data['status'] = false;
                    // $data['data'] = '';

                    switch ($response->getStatusCode()) {
                        case '400':
                            $data['message'] = 'Invalid invoice id supplied.';
                        break;
                        case '404':
                            $data['message'] = 'Invoice not found.';
                        break;
                        
                        default:
                            $data['message'] = (!empty(json_decode($response->getBody()->getContents())->message)) ? json_decode($response->getBody()->getContents(), true)->message : 'Invoice not found.';
                        break;
                    }
                    return response()->json(['result' => $data], $response->getStatusCode());
                }

            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    /**
    GET ALL INVOICES 
    **/
    public function getAllInvoices($offset, $limit, $param = false){

        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $credentials    = getUserFromToken($headers['token']);
            if($credentials){
                $endpoint      = env('API_BASE_URL').'/invoices/pagination?offset='.$offset.'&limit='.$limit.'&withItems=false&audit=NONE';
                try{
                    
                    $client    = new Client();
                    $response  = $client->get($endpoint, ['auth'=> $credentials, 'headers'=> $this->headers]);
                    
                    $data['status']     = true;
                    $data['message']    = 'Record Found';
                    $data['data']               = json_decode($response->getBody()->getContents(), true);

                    $data['total_records']      = $response->getHeader('x-killbill-pagination-totalnbrecords')[0];
                    $data['next_page_url']      = @$response->getHeader('x-killbill-pagination-nextpageuri')[0];
                    $data['max_records_showing']= $response->getHeader('x-killbill-pagination-maxnbrecords')[0];
                    $data['current_offset']     = $response->getHeader('x-killbill-pagination-currentoffset')[0];
                    $data['next_offset']        = @$response->getHeader('x-killbill-pagination-nextoffset')[0];

                    if($param){
                        return $data['data'];
                    }else{
                        return response()->json(['result' => $data], $response->getStatusCode());
                    }

                }catch(ClientException $e){

                    $response           = $e->getResponse();
                    $data['status']     = false;
                    $msg                = json_decode($response->getBody()->getContents());
                    $data['message']    = ($msg) ? $msg->message : 'Some exception occured.';
                    // $data['data']       = '';
                    if($param){
                        return $data['data'] = '';
                    }else{
                        return response()->json(['result' => $data], $response->getStatusCode());
                    }
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    public function addCustomInvoice(Request $request){

        // $todayPreviousDate = date('Y-m-d', strtotime(' -1 day'));
        // $todayDate = date('Y-m-d', strtotime('+2 day'));
        $todayDate = date('Y-m-d', time());


        $accountId = $request->accountId;
        $autoCommit = $request->autoCommit;

        if (!$autoCommit) {
            $autoCommit = "false";
        }else{
            $autoCommit = "true";
        }
        // return $autoCommit;
        //get invoice on todays date aginst account id
        $endpoint = env('API_BASE_URL')."/accounts/$accountId/invoices?startDate=$todayDate";
        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $userCredentials = getUserFromToken($token);

            if($userCredentials){
                try{
                    $client = new Client();
                    $client2 = new Client();
                    $response = $client->get($endpoint, ['auth'=> $userCredentials, 'headers'=> $this->headers]);
                    $invoices = json_decode($response->getBody()->getContents(), true);

                    // return $invoices;
                    $invoiceId = '';
                    if (!empty($invoices)) {

                        $draftedInvoiceArray = $this->findDratInvoice($invoices, $todayDate);
                        // return $draftedInvoiceArray;
                        $invoiceId = @$draftedInvoiceArray['invoiceId'];
                    }
                    // return $invoiceId;
                    $bodyData = [
                                    'invoiceId'     => $invoiceId,
                                    'accountId'     => $accountId,
                                    'productName'   => $request->product,
                                    'itemType'      => "EXTERNAL_CHARGE",
                                    'description'   => $request->description,
                                    // 'invoiceDate'   => $todayDate,
                                    'startDate'     => $todayDate,
                                    'currency'      => $request->currency,//
                                    'quantity'      => $request->quantity,
                                    'rate'          => $request->rate,
                                    'amount'        => $request->amount,
                                    'itemDetails'        => $request->unit
                                ];
                    $externalChargeEndpoint = env('API_BASE_URL')."/invoices/charges/$accountId?autoCommit=$autoCommit";
                    $requestOptions = [
                                        'auth'      => $userCredentials,
                                        'headers'   => $this->headers,
                                        'json'      => [$bodyData],
                                     ];
                    // return $externalChargeEndpoint;
                    $postResponse = $client2->post($externalChargeEndpoint, $requestOptions);

                    // $data = json_decode($postResponse->getBody()->getContents(), true);
                    // return $data;
                    $message = "Ad hoc attendance added successfully.";
                    if ((int) $postResponse->getStatusCode() > 199 && (int) $postResponse->getStatusCode() < 300) {
                        $data['status'] = true;
                        $data['message'] = $message;
                        $data['data'] = $bodyData;

                        return response()->json(['result' => $data], $response->getStatusCode());                 
                    } 

                }catch(ClientException $e){
                    
                    $response = $e->getResponse();
                    $data['status'] = false;

                    switch ($response->getStatusCode()) {
                        case '400':
                            $data['message'] = 'Invalid account id supplied.';
                        break;
                        case '404':
                            $data['message'] = 'Account not found.';
                        break;
                        
                        default:
                            $data['message'] = (!empty(json_decode($response->getBody()->getContents())->message)) ? json_decode($response->getBody()->getContents(), true)->message : 'Invoice not found.';
                        break;
                    }
                    return response()->json(['result' => $data], $response->getStatusCode());
                }

            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    /*public function addCustomInvoiceAdhoc(Request $request){

        $validator = Validator::make($request->all(), [
            'accountId' => 'required',
            'productTitle' => 'required',
            'notes' => 'required|sometimes',
            'quantity' => 'required|numeric',
            'unitPrice' => 'required',
            'total' => 'required',
            'unit' => 'required'

        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $accountId = $request->accountId;
        $currency  = ($request->currency)?:'ZAR';

        $todayDate = date('Y-m-d', time());
        
        $autoCommit = "false";

        $headers = $request->header();
        if(array_key_exists('token', $headers)){
            $userCredentials    = getUserFromToken($headers['token']);
            if($userCredentials){
                $bodyData = [
                                'accountId'     => $accountId,
                                'productName'   => $request->productTitle,
                                'itemType'      => "EXTERNAL_CHARGE",
                                'description'   => $request->notes,
                                // 'invoiceDate'   => $todayDate,
                                'startDate'     => $todayDate,
                                'currency'      => $currency,//
                                'quantity'      => $request->quantity,
                                'rate'          => $request->unitPrice,
                                'amount'        => $request->total,
                                'itemDetails'   => $request->unit
                            ];

                $requestOptions = [
                                    'auth'      => $userCredentials,
                                    'headers'   => $this->headers,
                                    'json'      => [$bodyData],
                                 ];

                // return ($requestOptions);

                $endpoint = env('API_BASE_URL')."/invoices/charges/$accountId?autoCommit=$autoCommit";

                try{
                    $client = new Client();
                    $response = $client->post($endpoint, $requestOptions);  

                    if ((int) $response->getStatusCode() > 199 && (int) $response->getStatusCode() < 300) {
                        $data['status'] = true;
                        $data['message'] = 'Ad hoc attendance added successfully.';
                        $data['data'] = $bodyData;

                        return response()->json(['result' => $data], $response->getStatusCode());                 
                    } 
                    
                }catch(ClientException $e){
                    
                    $response = $e->getResponse();
                    $data['status'] = false;

                    switch ($response->getStatusCode()) {
                        case '400':
                            $data['message'] = 'Invalid account id supplied.';
                        break;
                        case '404':
                            $data['message'] = 'Account not found.';
                        break;
                        
                        default:
                            $data['message'] = (!empty(json_decode($response->getBody()->getContents())->message)) ? json_decode($response->getBody()->getContents(), true)->message : 'Invoice not found.';
                        break;
                    }
                    return response()->json(['result' => $data], $response->getStatusCode());
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }*/

    public function addCustomInvoiceAdhoc(Request $request){

        $validator = Validator::make($request->all(), [
            'accountId' => 'required',
            'productTitle' => 'required',
            'notes' => 'required|sometimes',
            'quantity' => 'required|numeric',
            'unitPrice' => 'required',
            'total' => 'required',
            'unit' => 'required'

        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $accountId = $request->accountId;
        $currency  = ($request->currency)?:'ZAR';
        $todayDate = date('Y-m-d', time());
        $autoCommit = "false";
       
        // return $autoCommit;
        //get invoice on todays date aginst account id
        $endpoint = env('API_BASE_URL')."/accounts/$accountId/invoices?startDate=$todayDate";
        // return $endpoint;
        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $userCredentials = getUserFromToken($token);

            if($userCredentials){
                try{
                    $client = new Client();
                    $client2 = new Client();
                    $response = $client->get($endpoint, ['auth'=> $userCredentials, 'headers'=> $this->headers]);
                    $invoices = json_decode($response->getBody()->getContents(), true);

                    // return $invoices;
                    $invoiceId = '';
                    if (!empty($invoices)) {

                        $draftedInvoiceArray = $this->findDratInvoice($invoices, $todayDate);
                        // return $draftedInvoiceArray;
                        $invoiceId = @$draftedInvoiceArray['invoiceId'];
                    }
                    // return $invoiceId;
                    

                    $bodyData = [
                                'accountId'     => $accountId,
                                'productName'   => $request->productTitle,
                                'itemType'      => "EXTERNAL_CHARGE",
                                'description'   => $request->notes,
                                // 'invoiceDate'   => $todayDate,
                                'startDate'     => $todayDate,
                                'currency'      => $currency,//
                                'quantity'      => $request->quantity,
                                'rate'          => $request->unitPrice,
                                'amount'        => $request->total,
                                'itemDetails'   => $request->unit
                            ];

                    $externalChargeEndpoint = env('API_BASE_URL')."/invoices/charges/$accountId?autoCommit=$autoCommit";
                    $requestOptions = [
                                        'auth'      => $userCredentials,
                                        'headers'   => $this->headers,
                                        'json'      => [$bodyData],
                                     ];
                    // return $requestOptions;
                    $postResponse = $client2->post($externalChargeEndpoint, $requestOptions);

                    // $data = json_decode($postResponse->getBody()->getContents(), true);
                    // return $data;
                    $message = "Ad hoc attendance added successfully.";
                    if ((int) $postResponse->getStatusCode() > 199 && (int) $postResponse->getStatusCode() < 300) {
                        $data['status'] = true;
                        $data['message'] = $message;
                        $data['data'] = $bodyData;

                        return response()->json(['result' => $data], $response->getStatusCode());                 
                    } 

                }catch(ClientException $e){
                    
                    $response = $e->getResponse();
                    $data['status'] = false;

                    switch ($response->getStatusCode()) {
                        case '400':
                            $data['message'] = 'Invalid account id supplied.';
                        break;
                        case '404':
                            $data['message'] = 'Account not found.';
                        break;
                        
                        default:
                            $data['message'] = (!empty(json_decode($response->getBody()->getContents())->message)) ? json_decode($response->getBody()->getContents(), true)->message : 'Invoice not found.';
                        break;
                    }
                    return response()->json(['result' => $data], $response->getStatusCode());
                }

            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    private function findDratInvoice($invoices, $date){
        $array_with_status = array_column($invoices, 'status');
        $array_with_targetDate = array_column($invoices, 'targetDate');
        
        $local_array = [];

        foreach ($array_with_status as $key => $value) {
            if ($value == 'DRAFT') {
                if($invoices[$key]["targetDate"] == $date){
                    $local_array[] = $invoices[$key];
                }
            }
        }

        $length = @count($local_array);
        return (!empty($local_array))? $local_array[($length-1)] : [];
    }

    public function commitInvoice(Request $request)
    {
        $headers = apache_request_headers();
        $invoiceId = $request->invoiceId;
        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);

            if($checkToken){

                $credentials    = getUserFromToken($headers['token']);

                $bodyData = [];
                $requestOptions = [
                    'auth'  => $credentials,
                    'headers' => $this->headers,
                    'json' => $bodyData,
                ];
                

                $endpoint = env('API_BASE_URL').'/invoices/'.$invoiceId.'/commitInvoice';
               
                try {
                    $client = new Client();
                    $response = $client->put($endpoint, $requestOptions);  
                    
                    if ($response->getStatusCode() >= 200 && $response->getStatusCode() <=299) { 

                        $data['status'] = true;
                        $data['message'] = "Invoice committed successfully.";

                        // return response()->json(['result' => $data], $response->getStatusCode());
                        return response()->json(['result' => $data]);
                    } 
                }
                catch (ClientException $e) {
                    $response           = $e->getResponse();
                    $data['status']     = false;
                    $msg                = json_decode($response->getBody()->getContents());
                    $data['message']    = ($msg) ? $msg->message : 'Some exception occured.';
                    // $data['data']       = '';

                    return response()->json(['result' => $data], $response->getStatusCode());
                }
                
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }
}
