<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//FOR GUZZLE
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Validator;

class SubscriptionController extends Controller
{
    public $successStatus   = 200;
    public $errorStatus     = 401;

    private $headers = [];

    public function __construct(){

        $this->headers['Accept']                = 'application/json';
        $this->headers['Content-Type']          = 'application/json';
        $this->headers['X-Killbill-ApiKey']     = env('API_KEY');
        $this->headers['X-Killbill-ApiSecret']  = env('API_SECRET');
        /*$this->headers['X-Killbill-ApiKey']     = 'country';
        $this->headers['X-Killbill-ApiSecret']  = 'pakistan';*/
        $this->headers['X-Killbill-CreatedBy']  = 'Fee Me Web';
    }

    public function index(){
        return response()->json('success', 200);
    }

    public function getAccountBundle($accountId, $param = false){

        $endpoint = env('API_BASE_URL').'/accounts/'.$accountId.'/bundles';  

        $headers = apache_request_headers();
        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);

            if($checkToken){

                try{
                    $client = new Client();
                    $response = $client->get($endpoint, ['auth'=> $checkToken, 'headers'=> $this->headers]);
                    $data['status']         = true;
                    $data['message']        = 'Bundle found successfully.';

                    $data['data'] = json_decode($response->getBody()->getContents(), true);

                    if($param){
                        return $data['data'];
                    }else{
                        return response()->json(['result' => $data], $response->getStatusCode());
                    }

                }catch(ClientException $e){
                    $response = $e->getResponse();
                    // dd($response->getBody()->getContents());
                    $data['status'] = false;
                    // $data['data'] = '';

                    switch ($response->getStatusCode()) {
                        case '400':
                            $data['message'] = 'Invalid account id supplied.';
                        break;
                        case '404':
                            $data['message'] = 'Account not found.';
                        break;
                        
                        default:
                            $data['message'] = (!empty(json_decode($response->getBody()->getContents())->message)) ? json_decode($response->getBody()->getContents(), true)->message : 'Bundle not found.';
                        break;
                    }

                    if($param){
                        return $data['data'] = '';
                    }else{
                        return response()->json(['result' => $data], $response->getStatusCode());
                    }
                    
                }

            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }
    
    public function addSubscription(Request $request){
        // return $request->all();
        $validator = Validator::make($request->all(), [
            'accountId' => 'required',
            'plan' => 'required'
        ]);



        // return $added_data;

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $headers = $request->header();
        if(array_key_exists('token', $headers)){

            $headerArray    = getUserFromToken($headers['token']);
            // return $headerArray;
            if($headerArray){
                $entitlementDate = (!empty($request->entitlementDate) ? date('Y-m-d', strtotime($request->entitlementDate)) : date('Y-m-d'));
                // $billingDate = date("Y-m-d", strtotime("+1 month", strtotime(date('Y-m-d'))));

                $bodyData = [
                            "accountId"         => $request->accountId,
                            "externalKey"       => str_random(5),
                            "planName"          => $request->plan,
                            // "priceList"         => "DEFAULT",
                            // "productCategory"   => "BASE",//assuming catalog only consists on BASE
                            "startDate"         => $entitlementDate,
                            "billingStartDate"  => $entitlementDate
                            
                ];
                // return $bodyData;
                $requestOptions = [
                        'auth'  => $headerArray,
                        'headers' => $this->headers,
                        'json' => $bodyData,
                     ];
                     // return $requestOptions;
                $endpoint = env('API_BASE_URL')."/subscriptions?requestedDate=$entitlementDate&entitlementDate=$entitlementDate&billingDate=$entitlementDate";
                // return $endpoint;
                try{
                    $client = new Client();
                    $response = $client->post($endpoint, $requestOptions);  
                    //POST /1.0/kb/subscriptions
                    $message = "Subscription created successfully";
                    if ($response->getStatusCode() >= 200 || $response->getStatusCode() <= 299) { 
                        
                        $accountsubscriptions = $this->getAccountBundle($request->accountId, true);

                        $planName = $request->plan;
                        $added_data = array();
                        
                        // return $accountsubscriptions;
                        if($accountsubscriptions){

                            $subscriptions_data = array_column($accountsubscriptions, 'subscriptions');

                            foreach($subscriptions_data as $subscription_row){
                                $subscription_plan_name = $subscription_row[0]['planName'];
                                if($planName == $subscription_plan_name){
                                    $added_data['subscriptionId'] = $subscription_row[0]['subscriptionId']; 
                                    $added_data['state'] = $subscription_row[0]['state']; 
                                }
                            }
                        }


                        $data['status'] = true;
                        $data['message'] = $message;
                        $data['data'] = $added_data;

                        return response()->json(['result' => $data], $response->getStatusCode());
                    } 
                    
                }catch(ClientException $e){
                    $response = $e->getResponse();
                    // dd($response);
                    $data['status'] = false;
                    // $data['data'] = '';
                    $content = json_decode($response->getBody()->getContents(), true);
                    $data['message'] = (!empty($content["message"])) ? $content["message"] : 'Subscription failed to create.';
                    return response()->json(['result' => $data], $response->getStatusCode());
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    public function addSubscriptionWithAddOnProducts(Request $request){

        $validator = Validator::make($request->all(), [
            'accountId' => 'required',
            'externalKey' => 'required',
            'productName' => 'required',
            'productCategory' => 'required',
            'billingPeriod' => 'required',
            'priceList' => 'required',

        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $headers = $request->header();
        if(array_key_exists('token', $headers)){
            $headerArray    = getUserFromToken($headers['token']);
            if($headerArray){
                // $entitlementDate = (!empty($request->entitlementDate) ? date('Y-m-d', strtotime($request->entitlementDate)) : '');
                $bodyData = [
                    "accountId"         => $request->accountId,
                    "externalKey"       => (!empty($request->externalKey) ? $request->externalKey : ''),
                    "productName"       => $request->productName,
                    "productCategory"   => $request->productCategory,
                    "billingPeriod"     => $request->billingPeriod,
                    "priceList"         => $request->priceList

                ];

                $array = array();
                $array[] = $bodyData;
                /*if (!empty($entitlementDate)) {
                    $bodyData["billingStartDate"]   = $entitlementDate;
                    $bodyData["chargedThroughDate"] = $entitlementDate;
                    $bodyData["startDate"]          = $entitlementDate;
                }*/

                $requestOptions = [
                    'auth'  => $headerArray,
                    'headers' => $this->headers,
                    'json' => $array,
                 ];

                // dd($requestOptions);

                $endpoint = env('API_BASE_URL').'/subscriptions/createEntitlementWithAddOns';
                try{
                    $client = new Client();
                    $response = $client->post($endpoint, $requestOptions);  
                    //POST /1.0/kb/subscriptions
                    $message = "Subscription created successfully";
                    if ($response->getStatusCode() >= 200 || $response->getStatusCode() <= 299) { 
                        $data['status'] = true;
                        $data['message'] = $message;
                        // $data['data'] = '';

                        return response()->json(['result' => $data], $response->getStatusCode());
                    } 
                    
                }catch(ClientException $e){
                    $response = $e->getResponse();
                    // dd($response);
                    $data['status'] = false;
                    // $data['data'] = '';
                    $content = json_decode($response->getBody()->getContents(), true);
                    $data['message'] = (!empty($content["message"])) ? $content["message"] : 'Subscription failed to create.';
                    return response()->json(['result' => $data], $response->getStatusCode());
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    // cancel subscription
    public function cancelSubscription(Request $request){

        $validator = Validator::make($request->all(), [
            'subscriptionId' => 'required'
        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $subscriptionId = $request->subscriptionId;

        $headers = $request->header();
        if(array_key_exists('token', $headers)){
            $headerArray    = getUserFromToken($headers['token']);
            if($headerArray){
                // $entitlementDate = (!empty($request->entitlementDate) ? date('Y-m-d', strtotime($request->entitlementDate)) : '');
                $bodyData = [
                    "subscriptionId"    => $subscriptionId
                ];

                $requestOptions = [
                    'auth'  => $headerArray,
                    'headers' => $this->headers,
                    'json' => $bodyData,
                 ];

                $endpoint = env('API_BASE_URL').'/subscriptions/'.$subscriptionId.'?callCompletion=false&callTimeoutSec=5&entitlementPolicy=IMMEDIATE&billingPolicy=END_OF_TERM&useRequestedDateForBilling=true';

                // return $endpoint;

                try{
                    $client = new Client();
                    $response = $client->delete($endpoint, $requestOptions);  
                    //POST /1.0/kb/subscriptions
                    $message = "Subscription cancelled successfully.";
                    if ($response->getStatusCode() >= 200 || $response->getStatusCode() <= 299) { 
                        $data['status'] = true;
                        $data['message'] = $message;
                        // $data['data'] = '';

                        return response()->json(['result' => $data]);

                        // return response()->json(['result' => $data], $response->getStatusCode());
                    } 
                }catch(ClientException $e){
                    $response = $e->getResponse();
                    $data['status'] = false;
                    $content = json_decode($response->getBody()->getContents(), true);
                    $data['message'] = (!empty($content["message"])) ? $content["message"] : 'Error in cancelling subscription.';
                    return response()->json(['result' => $data], $response->getStatusCode());
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    // uncancel subscription
    public function unCancelSubscription(Request $request){

        $validator = Validator::make($request->all(), [
            'subscriptionId' => 'required'
        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $subscriptionId = $request->subscriptionId;

        $headers = $request->header();
        if(array_key_exists('token', $headers)){
            $headerArray    = getUserFromToken($headers['token']);
            if($headerArray){
                $bodyData = [
                    "subscriptionId"    => $subscriptionId
                ];

                $requestOptions = [
                    'auth'  => $headerArray,
                    'headers' => $this->headers,
                    'json' => $bodyData,
                 ];

                $endpoint = env('API_BASE_URL').'/subscriptions/'.$subscriptionId.'/uncancel';

                try{
                    $client = new Client();
                    $response = $client->put($endpoint, $requestOptions);  
                    //POST /1.0/kb/subscriptions
                    $message = "Subscription activated successfully.";
                    if ($response->getStatusCode() >= 200 || $response->getStatusCode() <= 299) { 
                        $data['status'] = true;
                        $data['message'] = $message;
                        // $data['data'] = '';

                        return response()->json(['result' => $data]);
                        // return response()->json(['result' => $data], $response->getStatusCode());
                    } 
                }catch(ClientException $e){
                    $response = $e->getResponse();
                    $data['status'] = false;
                    $content = json_decode($response->getBody()->getContents(), true);
                    $data['message'] = (!empty($content["message"])) ? $content["message"] : 'Error in activating subscription.';
                    return response()->json(['result' => $data], $response->getStatusCode());
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

}
