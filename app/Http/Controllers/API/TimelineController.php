<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Validator;

class TimelineController extends Controller
{
	//test comment
	public $successStatus   = 200;
    public $errorStatus     = 401;
    
	private $headers = [
                        'Accept'                => 'application/json',
                        'Content-Type'          => 'application/json',
                        'X-Killbill-CreatedBy'  => 'Fee Me Web'
                       ];
    private $credential   = [];
    
    public function __construct(){
	    $this->headers['X-Killbill-ApiKey'] 	= env('API_KEY');
	    $this->headers['X-Killbill-ApiSecret'] 	= env('API_SECRET');
	}
	public function index($accountId){
    	$headers = apache_request_headers();
    	if(!array_key_exists('token', $headers)){
    		$data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
    	}

    	$credentials   = getUserFromToken($headers['token']);
    	$endpoint      = env('API_BASE_URL')."/accounts/$accountId/timeline";

        try{
            $client    = new Client();
            $response  = $client->get($endpoint, ['auth'=> $credentials, 'headers'=> $this->headers]);
            
            $data['data'] 		= json_decode($response->getBody()->getContents(), true);
            $data['status']     = true;
            $data['message']    = 'Timeline found successfully.';

            return response()->json(['result' => $data], $response->getStatusCode());
        }catch(ClientException $e){

            $response           = $e->getResponse();
            $content            = json_decode($response->getBody()->getContents(), true);
            $data['message']    = (!empty($content["message"])) ? $content["message"] : 'Timeline not found.';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }
}
