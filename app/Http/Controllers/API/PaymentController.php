<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//FOR GUZZLE
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;

class PaymentController extends Controller
{
    public $successStatus   = 200;
    public $errorStatus     = 401;

	private $headers = [
                            'Accept'                => 'application/json',
                            'Content-Type'          => 'application/json',
                            'X-Killbill-CreatedBy'  => 'Fee Me Web'
                       ];
    private $credential   = [];
    
    public function __construct(){
	    $this->headers['X-Killbill-ApiKey'] 	= env('API_KEY');
	    $this->headers['X-Killbill-ApiSecret'] 	= env('API_SECRET');
	}
    public function index($accountId){
    	$headers = apache_request_headers();
    	if(!array_key_exists('token', $headers)){
    		$data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
    	}

    	$credentials   = getUserFromToken($headers['token']);
    	$endpoint      = env('API_BASE_URL')."/accounts/$accountId/invoicePayments";
    	// return  $this->headers;
        try{
            $client    = new Client();
            $response  = $client->get($endpoint, ['auth'=> $credentials, 'headers'=> $this->headers]);
            
            $data['data'] 		= json_decode($response->getBody()->getContents(), true);
            $data['status']     = true;
            $data['message']    = 'Invoice payments found successfully.';

            return response()->json(['result' => $data], $response->getStatusCode());
        }catch(ClientException $e){

            $response           = $e->getResponse();
            $content            = json_decode($response->getBody()->getContents(), true);
            $data['message']    = (!empty($content["message"])) ? $content["message"] : 'Invoice payments not found.';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
        
    }

    /**
    GET ALL PAYMENTS 
    **/
    public function getAllPayments($offset, $limit, $param = false){

        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $credentials    = getUserFromToken($headers['token']);
            if($credentials){
                $endpoint      = env('API_BASE_URL').'/payments/pagination?offset='.$offset.'&limit='.$limit.'&audit=NONE&withPluginInfo=false&withAttempts=false';
                try{
                    
                    $client    = new Client();
                    $response  = $client->get($endpoint, ['auth'=> $credentials, 'headers'=> $this->headers]);
                    
                    $data['status']     = true;
                    $data['message']    = 'Record Found';
                    $data['data']               = json_decode($response->getBody()->getContents(), true);

                    $data['total_records']      = $response->getHeader('x-killbill-pagination-totalnbrecords')[0];
                    $data['next_page_url']      = @$response->getHeader('x-killbill-pagination-nextpageuri')[0];
                    $data['max_records_showing']= $response->getHeader('x-killbill-pagination-maxnbrecords')[0];
                    $data['current_offset']     = $response->getHeader('x-killbill-pagination-currentoffset')[0];
                    $data['next_offset']        = @$response->getHeader('x-killbill-pagination-nextoffset')[0];

                    if($param){
                        return $data['data'];
                    }else{
                        return response()->json(['result' => $data], $response->getStatusCode());
                    }

                }catch(ClientException $e){

                    $response           = $e->getResponse();
                    $data['status']     = false;
                    $msg                = json_decode($response->getBody()->getContents());
                    $data['message']    = ($msg) ? $msg->message : 'Some exception occured.';
                    // $data['data']       = '';
                    if($param){
                        return $data['data'] = '';
                    }else{
                        return response()->json(['result' => $data], $response->getStatusCode());
                    }
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }
}
