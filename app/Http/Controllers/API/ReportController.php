<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//FOR GUZZLE
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Validator;

class ReportController extends Controller
{
    public $successStatus   = 200;
    public $errorStatus     = 401;
    public function __construct(){

        $this->headers['Accept']                = 'application/json';
        $this->headers['Content-Type']          = 'application/json';
        $this->headers['X-Killbill-ApiKey']     = env('API_KEY');
        $this->headers['X-Killbill-ApiSecret']  = env('API_SECRET');
        $this->headers['X-Killbill-CreatedBy']  = 'Fee Me Web';
    }

    public function getReport($reportName, $smoothName = null, $startDate = null, $endDate = null, $format = 'json'){
    	
        $startDate  = ($smoothName)? $smoothName: '';
        // $startDate  = ($startDate)? date('Y-m-d', time()): date('Y-m-d', strtotime("-3 months"));
        // $endDate    = ($endDate)? date('Y-m-d', time()): date('Y-m-d', time());
        $startDate  = date('Y-m-d', strtotime("-3 months"));
        $endDate    = date('Y-m-d', time());
    	// $endDate = "2018-10-14";

        $headers = apache_request_headers();
        //check if we got token
        if(!array_key_exists('token', $headers)){
        	$data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
        //check if token exist in table
        $credentials    = getUserFromToken($headers['token']);
        if(empty($credentials)){
            $data['status'] = false;
            $data['message'] = "Invalid Token.";

            return response()->json(['result' => $data], $this->errorStatus);
        }


        switch ($reportName) {
            case 'report_accounts_summary':
            case 'report_new_accounts_daily':
                $partialEndpoint = "?name=$reportName&format=$format&smooth=$smoothName&startDate=$startDate&endDate=$endDate";
            break;
            case 'report_invoices_daily':
                $partialEndpoint = "?name=$reportName^metric:original_amount_charged^dimension:currency&format=$format&smooth=$smoothName&startDate=$startDate&endDate=$endDate";
            break;
            case 'report_payments_total_daily':
                $partialEndpoint = "?name=$reportName^metric:invoice_amount_paid^dimension:currency&format=$format&smooth=$smoothName&startDate=$startDate&endDate=$endDate";
            break;
        }

        $endpoint = env('REPORT_BASE_URL').$partialEndpoint;
        // return $endpoint;
        try{
            // return $startDate;
            $client    = new Client();
            $response  = $client->get($endpoint, ['auth'=> $credentials, 'headers'=> $this->headers]);
            
            $data['status']     = true;
            $data['message']    = 'Report data Found';
            $reponseData      = json_decode($response->getBody()->getContents(), true);
            // return 
            $chartData = $reponseData;
            switch ($reportName) {
            	case 'report_accounts_summary':

        			if (!empty($reponseData)) {
	            	$chartData = $reponseData[0]["data"];
	            	if (!empty($chartData)) {
		            		foreach ($chartData as $key => $value) {
		            			$chartData[$key]["name"] = $value["label"];
		            			$chartData[$key]["y"]    = $value["value"];
		            			unset($chartData[$key]["label"]);
		            			unset($chartData[$key]["value"]);
		            		}
		            	}
		            }
		            $data["data"] = $chartData;
        		break;

        		case 'report_new_accounts_daily':
                    if (!empty($chartData)) {
                        $all_xValues = [];
                        $all_yValues = [];
                        foreach ($chartData[0]["data"][0]["values"] as $key => $value) {
                            $all_xValues[] = $chartData[0]["data"][0]["values"][$key]["x"] = date("Y-m-d", strtotime($value["x"]));
                            $all_yValues[] = $value["y"];
                        }
                    }
                    $chartData[0]["data"][0]["x_values"] = $all_xValues;
                    $chartData[0]["data"][0]["y_values"] = $all_yValues;
                    return $data["data"] = $chartData[0];
                break;
        		case 'report_invoices_daily':
                case 'report_payments_total_daily':
                    // return $chartData;
        			if (!empty($chartData)) {
                        foreach ($chartData[0]["data"] as $main_key => $main_value) {
                            $all_xValues = [];
                            $all_yValues = [];

                            $x_values = array_column($chartData[0]["data"][$main_key]["values"], 'x');
                            $y_values = array_column($chartData[0]["data"][$main_key]["values"], 'y');

                            foreach ($x_values as $key => $value) {
                                $x_values[$key] = date("Y-m-d", strtotime($value));
                            }

                            $chartData[0]["data"][0]["x_values"] = $x_values;
                            $chartData[0]["data"][$main_key]["y_values"] = $y_values;
                        }
        			}
        			
        			return $data["data"] = $chartData[0];
        		break;
            }
            
            return response()->json(['result' => $data], $response->getStatusCode());

        }catch(ClientException $e){

            $response           = $e->getResponse();
            $data['status']     = false;
            $msg                = json_decode($response->getBody()->getContents());
            $data['message']    = ($msg) ? $msg->message : 'Some exception occured.';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }
}
