<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Validator;
use App\User;

class AuthController extends Controller
{
	public $successStatus   = 200;
    public $errorStatus     = 401;

    private $headers = [];
    private $twilioHeaders = [];
    private $credential = [];

    public function __construct(){

        $this->headers['Accept']                = 'application/json';
        $this->headers['Content-Type']          = 'application/json';
        $this->headers['X-Killbill-ApiKey']     = env('API_KEY');
        $this->headers['X-Killbill-ApiSecret']  = env('API_SECRET');
        $this->headers['X-Killbill-CreatedBy']  = 'Fee Me Web';

        $this->twilioHeaders['X-Authy-API-Key'] = env('TWILIO_API_KEY');
    }

	public function login(Request $request)
    {
        $email      = $request->email;
        $password   = $request->password;
        $this->credential[] = $email;
        $this->credential[] = $password;

        $validator = Validator::make($request->all(), [
            'email'     => 'required|email',
            'password'  => 'required',
        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }
        // return $this->credential;
        try {
            $endpoint = env('API_BASE_URL').'/security/subject';
            $endpoint2 = env('API_BASE_URL').'/security/subject/info';
            $client = new Client();
            $response = $client->get($endpoint, ['auth'=> $this->credential, 'headers'=> $this->headers]);
            $response2 = $client->get($endpoint2, ['auth'=> $this->credential, 'headers'=> $this->headers]);

            if ($response->getStatusCode() >= 200 && $response->getStatusCode() <=299) { 
                // return $response->getBody()->getContents();
                $data['status']     = true;
                $data['message']    = "Credentials matched.";
                $data['data']       = json_decode($response->getBody()->getContents(), true);
                $extraData          = json_decode($response2->getBody()->getContents(), true);

                $data['data'] = array_merge($data['data'], $extraData);

                $encoded_data = base64_encode($request->email.':'.$request->password);
                $token = create_user($encoded_data);

                $data['data']['token'] = $token;

                return response()->json(['result' => $data], $response->getStatusCode());
            } 
        }
        catch (ClientException $e) {
            $obj = (object)[] ;
            $response = $e->getResponse();
            if (!empty($response) && isset($response)) {
                $data['status'] = false;
                $data['message'] = $e->getResponse()->getReasonPhrase().' user or password.';
                $data['data'] = $obj;
            }else{
                $data['status'] = false;
                $data['message'] = 'Email or Password does\'nt match.'; 
            }
            return response()->json(['result' => $data], $response->getStatusCode());
            
        }
    }

    public function signup(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email'         => 'required|email',
            'name'          => 'required',
            'surname'       => 'required',
            'mobile_number' => 'required|numeric',
            'password'      => 'required|string|min:6',

        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $bodyData = ['username' => $request->email, 'password' => $request->password, 'name' => $request->name, 'surname' => $request->surname, 'mobileNumber' => $request->mobile_number, "roles" => ["all_permissions"]];
        $requestOptions = [
                'auth'  => ['admin', 'password'],
                'headers' => $this->headers,
                'json' => $bodyData
             ];

         // return $bodyData;
         // return $requestOptions;
        $endpoint = env('API_BASE_URL').'/security/users';

        try {
            $client = new Client();
            $response = $client->post($endpoint, $requestOptions);  
            
            if ($response->getStatusCode() >= 200 && $response->getStatusCode() <=299) { 
                $data['status'] = true;
                $data['message'] = "User created successfully.";
                $data['data'] = $request->all();

                return response()->json(['result' => $data], $response->getStatusCode());
            } 
        }
        catch (ClientException $e) {
            $response = $e->getResponse();
            $raw_data = $response->getBody()->getContents();

            $final_data = json_decode($raw_data, true);
            // return $final_data['message'];

            if (!empty($final_data['message'])) {
                // dd($content);
                $content = $final_data['message'];
            }else{
                $content = 'User exists.';
            }
            $data['status'] = false;
            $data['message'] = $content;
            // $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }

    public function sendOtp(Request $request)
    {
        $bodyData = ['via' => 'sms', 'country_code' => $request->country_code, 'phone_number' => $request->phone_number];
        $requestOptions = [
                'headers' => $this->twilioHeaders,
                'json' => $bodyData
             ];

        $endpoint = "https://api.authy.com/protected/json/phones/verification/start";

        try {
            $client = new Client();
            $response = $client->post($endpoint, $requestOptions);  
            
            if ($response->getStatusCode() >= 200 && $response->getStatusCode() <=299) { 
                $data['status'] = true;
                $data['message'] = "OTP sent successfully.";

                return response()->json(['result' => $data], $response->getStatusCode());
            } 
        }catch (ClientException $e) {
            $response = $e->getResponse();
            $raw_data = $response->getBody()->getContents();

            $content = json_decode($raw_data, true);
            $data["message"] = (!empty($content['message'])) ? $content['message'] :  'OTP not sent.';
            $data['status'] = false;

            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }
    public function verifyOtp(Request $request)
    {

        try {
            $endpoint = "https://api.authy.com/protected/json/phones/verification/check?country_code=$request->country_code&phone_number=$request->phone_number&verification_code=$request->verification_code";

            $client = new Client();
            $response = $client->get($endpoint, ['headers'=> $this->twilioHeaders]);

            if ($response->getStatusCode() >= 200 && $response->getStatusCode() <=299) { 
                // return $response->getBody()->getContents();
                $data['status'] = true;
                $data['message'] = "OTP matched.";
                $data['data'] = json_decode($response->getBody()->getContents(), true);

                return response()->json(['result' => $data], $response->getStatusCode());
            } 
        }catch (ClientException $e) {
            $response = $e->getResponse();
            $raw_data = $response->getBody()->getContents();

            $content = json_decode($raw_data, true);
            $data["message"] = (!empty($content['message'])) ? $content['message'] :  'OTP not matched.';
            $data['status'] = false;

            return response()->json(['result' => $data], $response->getStatusCode());            
        }
    }

    public function updatePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => 'required', 
            'password' => 'required|confirmed|min:6'            
        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);

            if($checkToken){

                if($request->current_password != $checkToken[1]){

                    $data['status'] = false;
                    $data['message'] = 'Your current password field is incorrect.';

                    return response()->json(['result'=>$data], $this->errorStatus);   
                }else{

                    $bodyData = ['username' => $checkToken[0], 'password' => $request->password];
                    $requestOptions = [
                        'auth'      => ['admin', 'password'],
                        'headers'   => $this->headers,
                        'json'      => $bodyData
                     ];
                    

                    $endpoint = env('API_BASE_URL').'/security/users/'.$checkToken[0].'/'.'password';
                    // return $endpoint;
                   
                    try {
                        $client = new Client();
                        $response = $client->put($endpoint, $requestOptions);  
                        
                        if ($response->getStatusCode() >= 200 && $response->getStatusCode() <=299) { 

                            // update token encoded_data
                            $encoded_data = base64_encode($checkToken[0].':'.$request->password);
                            $update_pass  = update_user_password($token, $encoded_data);

                            $data['status'] = true;
                            $data['message'] = "Password updated successfully.";

                            return response()->json(['result' => $data], $response->getStatusCode());
                        } 
                    }
                    catch (ClientException $e) {
                        $response           = $e->getResponse();
                        $data['status']     = false;
                        $msg                = json_decode($response->getBody()->getContents());
                        $data['message']    = ($msg) ? $msg->message : 'Some exception occured.';
                        // $data['data']       = '';

                        return response()->json(['result' => $data], $response->getStatusCode());
                    }
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    public function logout(Request $request)
    {
        $headers = apache_request_headers();
        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);
            if($checkToken){
                
                $user           = User::where('token' , $token)->firstOrFail();
                $user->delete();
                $data['status'] = true;
                $data['message'] = "User logged out successfully.";

                return response()->json(['result' => $data], $this->successStatus);
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    public function updateEmailConfig(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'imapUsername'  => 'required',
            'imapPassword'  => 'required',
            'imapHost'      => 'required'
        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);

            if($checkToken){

                $credentials    = getUserFromToken($headers['token']);
                $email = $credentials[0];

                $bodyData = [
                    "imapUsername"      => $request->imapUsername,
                    "imapPassword"      => $request->imapPassword,
                    "imapHost"          => $request->imapHost
                ];
                $requestOptions = [
                    'auth'  => $credentials,
                    'headers' => $this->headers,
                    'json' => $bodyData,
                ];
                return $requestOptions;

                $endpoint = env('API_BASE_URL').'/security/users/'.$email.'/emailconfig';

                try {
                    $client = new Client();
                    $response = $client->put($endpoint, $requestOptions);  
                    
                    if ($response->getStatusCode() >= 200 && $response->getStatusCode() <=299) { 

                        // update token encoded_data
                        $encoded_data = base64_encode($checkToken[0].':'.$request->password);

                        $data['status'] = true;
                        $data['message'] = "Email Configuration updated successfully.";

                        return response()->json(['result' => $data]);
                    } 
                }
                catch (ClientException $e) {
                    $response           = $e->getResponse();
                    $data['status']     = false;
                    $msg                = json_decode($response->getBody()->getContents());
                    $data['message']    = ($msg) ? $msg->message : 'Some exception occured.';

                    return response()->json(['result' => $data]);
                }
                
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    public function loggedInUser()
    {
        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);

            if($checkToken){


                $endpoint = env('API_BASE_URL').'/security/subject/info';

                try{
                    $client = new Client();
                    $response = $client->get($endpoint, ['auth'=> $checkToken, 'headers'=> $this->headers]);
                    $data['status']         = true;
                    $data['message']        = 'User information found successfully.';

                    $data['data'] = json_decode($response->getBody()->getContents(), true);

                    return response()->json(['result' => $data], $response->getStatusCode());
                }catch(ClientException $e){
                    
                    $response           = $e->getResponse();
                    $data['status']     = false;
                    $msg                = json_decode($response->getBody()->getContents());
                    $data['message']    = ($msg) ? $msg->message : 'Some exception occured.';
                   
                    return response()->json(['result' => $data], $response->getStatusCode());
                }
                
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }
}
