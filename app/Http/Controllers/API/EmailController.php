<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

//FOR GUZZLE
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Validator;

use Illuminate\Support\Facades\DB;

use App\Subscription;

use PhpImap;

class EmailController extends Controller
{
	public $successStatus   = 200;
    public $errorStatus     = 401;

    private $headers = [];
    private $inboxHost;
    private $sentHost;

    private $mailbox_host;
    private $mailbox_user_name;
    private $mailbox_user_password;

    public function __construct(){

        $this->headers['Accept']                = 'application/json';
        $this->headers['Content-Type']          = 'application/json';
        $this->headers['X-Killbill-ApiKey']     = env('API_KEY');
        $this->headers['X-Killbill-ApiSecret']  = env('API_SECRET');
        $this->headers['X-Killbill-CreatedBy']  = 'Fee Me Web';
    }

    public function get_account_subscriptions($credentials){
        $endpoint = env('API_BASE_URL').'/accounts/paginationwithbundles?offset=0&limit=1000000';
        try{
            $client    = new Client();
            $response  = $client->get($endpoint, ['auth'=> $credentials, 'headers'=> $this->headers]);
            $accountWithSubscriptions = json_decode($response->getBody()->getContents(), true);

            /*if (empty($accountWithSubscriptions)) {
                return false;
            }*/

            $new_accounts_array = [];
            foreach ($accountWithSubscriptions as $key => $value) {
                if (!empty($value["bundles"])) {
                    foreach ($value["bundles"] as $bundle_key => $bundle_value) {
                        if (!empty($bundle_value['subscriptions'])) {
                            foreach ($bundle_value['subscriptions'] as $subscription_key => $subscription_value) {
                                // $accountWithSubscriptions[$key]["subscriptions"][] = $subscription_value;
                                $productName = $subscription_value["productName"];
                                $state = strtoupper($subscription_value["state"]);
                                if ((strtoupper($productName) == 'EMAIL' || strtoupper($productName) == 'EMAILS') && $state == 'ACTIVE') {
                                    $accountWithSubscriptions[$key]['email_subscriptionId'] = $subscription_value['subscriptionId'];
                                }
                                
                            }
                        }
                        
                    }
                }                
            }

            return $accountWithSubscriptions;


        }catch(ClientException $e){
            return [];
        }
        return [];
    }
    public function getEmails(){
        
        // $credentials    = getUserFromToken('ZVwgDYAwusTHzH8QYcIH');//$headers['token']
        $credentials    = ["admin", "password"];//$headers['token']
        // return $credentials;
        if (empty($credentials)) {
            return "Logged in user credentials not found...";
        }

        $getSettings    = env('API_BASE_URL').'/security/subject/info';
        
        try{
            $client    = new Client();
            $settings  = $client->get($getSettings, ['auth'=> $credentials, 'headers'=> $this->headers]);
            
            /**************************************** GET ACCOUNTS WITH SUBSCRIPTION ID ***************************************/

            $settings = json_decode($settings->getBody()->getContents(), true);
            if (empty($settings["imapUsername"]) && empty($settings["imapPassword"]) && empty($settings["imapHost"])) {
                return false;
            }
            $this->mailbox_host             = $settings["imapHost"];
            $this->mailbox_user_name        = $settings["imapUsername"];
            $this->mailbox_user_password    = $settings["imapPassword"];
            $this->inboxHost                = $this->mailbox_host."INBOX";
            $this->sentHost                 = $this->mailbox_host."INBOX.Sent";
            // dd($settings);
            
            $accountWithSubscriptions = $this->get_account_subscriptions($credentials); 
            if (empty($accountWithSubscriptions)) {
               return false;
            }          
            /**************************************** GET ACCOUNTS WITH SUBSCRIPTION ID ***************************************/



        }catch(ClientException $e){

            $response           = $e->getResponse();
            $content            = json_decode($response->getBody()->getContents(), true);
            $data['message']    = (!empty($content["message"])) ? $content["message"] : 'Not found.';

            return response()->json(['result' => $data], $response->getStatusCode());
        }

        /**************************************** GET ACCOUNTS WITH SUBSCRIPTION ID ***************************************/

            $date_from  = date('Y-m-d', time());
            $date_to    = date('Y-m-d H:i:s', strtotime(' +1 day '));//this date before so we need +1 day
            $user_email = $this->mailbox_user_name;
            $user_pwd   = $this->mailbox_user_password;
            
            
            $since_date     = date('d-M-Y', strtotime($date_from));
            $before_date    = date('d-M-Y', strtotime($date_to));

            $search_string = '';
            if($since_date){
                $search_string .= ' SINCE '. '"'.$since_date.'"';
            }
            if($before_date){
                $search_string .= ' BEFORE '. '"'.$before_date.'"';
            }


            if( isset($date_from) && isset($date_to) && isset($user_email) && isset($user_pwd) ){

                $endpointNew     = env('API_BASE_URL').'/usages';
                $clientNew    = new Client();

                $inboxArray = $this->get_inbox($user_email, $user_pwd, $search_string);
                $sentArray  = $this->get_sent($user_email, $user_pwd, $search_string);//yet this is empty we'll do it in future

                $inbox_sent_emails = array_merge($inboxArray, $sentArray);
                // dd($inbox_sent_emails);
                if (empty($inbox_sent_emails)) {
                    return false;
                }

                $requestOptionsNew = [
                                    'auth'      => $credentials,
                                    'headers'   => $this->headers,
                                    'json'      => '',
                                ];

                foreach ($inbox_sent_emails as $inbox_key => $inbox_value) {
                    
                    $data = $this->get_attendance_data($inbox_value, $accountWithSubscriptions, $credentials);
                    if (empty($data)) {
                        $record = Subscription::where('email_message_id', $inbox_value->message_id)->first();
                        if (empty($record)) {

                            $mailbox_from_email = $this->get_from_email($inbox_value->from);
                            $mailbox_to_email = $this->get_to_email($inbox_value->to);

                            $input = [];
                            $input['type']              = 'Email';
                            $input['account_id']        = '';
                            $input['subscription_id']   = '';
                            $input['number']           = ($mailbox_from_email == $user_email)? $mailbox_to_email : $mailbox_from_email;
                            $input['incoming']          = ($mailbox_from_email == $user_email)? 0 : 1;
                            $input['count']             = 1;
                            $input['notes']             = $inbox_value->subject;
                            $input['email_message_id']  = $inbox_value->message_id;
                            $input['date']              = date('Y-m-d H:i:s', strtotime($inbox_value->date));

                            Subscription::create($input);
                        }
                    }
                    
                    
                }
                
                // dd($test);
               return 'Job successfully done.';

            }else{
                $data['message']    = 'Please provide all the required parameters';
                $data['status']     = false;
                return response()->json(['result' => $data], $this->errorStatus);
            }

    }


    private function get_inbox($user_email, $user_pwd, $search_string){
        // dd($this->inboxHost."   ".$user_email."   ".$user_pwd);
        $mailbox = new PhpImap\Mailbox("$this->inboxHost", $user_email, $user_pwd , __DIR__);

        // Read all messaged into an array:
        $inboxArray     = array();
        $mailsIds = $mailbox->searchMailbox($search_string);
        if(!$mailsIds) {
            return  $inboxArray;
        }

        $emailHeaders = $mailbox->getMailsInfo($mailsIds);

        return $emailHeaders;
    }

    private function get_sent($user_email, $user_pwd, $search_string){


        $inboxSentArray     = array();
        $mailbox = new PhpImap\Mailbox("$this->sentHost", $user_email, $user_pwd , __DIR__);

        // Read all messaged into an array:
        $mailsIds = $mailbox->searchMailbox($search_string);
        if(!$mailsIds) {
            return  $inboxSentArray;
        }
        $emailHeaders = $mailbox->getMailsInfo($mailsIds);
        return $emailHeaders;
    }
    private function get_from_email($mailbox_from_email){
        if (strpos($mailbox_from_email, '<')) {
            $response1 = explode('<', $mailbox_from_email);
            $mailbox_from_email = explode('>', $response1[1]);
            $mailbox_from_email = $mailbox_from_email[0];
        }

        return $mailbox_from_email;
    }
    private function get_to_email($mailbox_to_email){
        if (strpos($mailbox_to_email, '<')) {
            $response1 = explode('<', $mailbox_to_email);
            $mailbox_to_email = explode('>', $response1[1]);
            $mailbox_to_email = $mailbox_to_email[0];
        }

        return $mailbox_to_email;
    }
    private function get_attendance_data($inbox_value, $accountWithSubscriptions, $credentials){
        // dd($inbox_value);

        $mailbox_from_email = $this->get_from_email($inbox_value->from);
        $mailbox_to_email = $this->get_to_email($inbox_value->to);


        
        
        $counter = 0;
        foreach ($accountWithSubscriptions as $key => $value) {
            $counter++;
            $kb_account_email = $value['email'];
            $accountId = '';
            $subscriptionId = '';
            $recordUsage = false;
            if (array_key_exists('email_subscriptionId', $value)) {
                $accountId = $value["accountId"];
                $subscriptionId = $value["email_subscriptionId"];
                $recordUsage = true;
            }

            $endpoint       = env('API_BASE_URL').'/usages';
            $requestOptions = [
                                'auth'  => $credentials,
                                'headers' => $this->headers,
                                'json' => "",
                            ];
            

            if ($kb_account_email == $mailbox_from_email) {
                //record incoming

                $record = Subscription::where('email_message_id', $inbox_value->message_id)->first();
                // dd($record);
                if (empty($record)) {
                    $input = [];
                    $input['type']              = 'Email';
                    $input['account_id']        = $accountId;
                    $input['subscription_id']   = $subscriptionId;
                    $input['number']            = $kb_account_email;
                    $input['incoming']          = 1;
                    $input['count']             = 1;
                    $input['notes']             = $inbox_value->subject;
                    $input['email_message_id']  = $inbox_value->message_id;
                    $input['date']              = date('Y-m-d H:i:s', strtotime($inbox_value->date));

                    Subscription::create($input);
                    
                    /*try{
                        $client = new Client();
                        $requestOptions['json'] = $dataArray;
                        $client->post($endpoint, $requestOptions);
                        $dataArray = [];
                    }catch(ClientException $e){

                        $response           = $e->getResponse();
                        $content            = json_decode($response->getBody()->getContents(), true);
                        $data['message']    = (!empty($content["message"])) ? $content["message"] : 'usage error.';
                        $data['counter'] = $counter;
                        $data['req'] = $requestOptions;

                        dd($data);
                    }*/
                    if($recordUsage){
                        // data array send to server
                        $dataArray = array(
                                    'subscriptionId'   => $input['subscription_id'],
                                    'unitUsageRecords' => array(array(
                                        'unitType'          => 'count',
                                        'usageRecords'      => array(array(
                                            'recordDate'        => date('Y-m-d', strtotime($inbox_value->date)),
                                            'amount'            => 1
                                        ))  
                                    ))
                                );

                        $client = new Client();
                        $requestOptions['json'] = $dataArray;
                        $client->post($endpoint, $requestOptions);
                    }
                    
                    
                    return $input;
                }

            }elseif ($kb_account_email == $mailbox_to_email) {
                //record outgoing                

                $record = Subscription::where('email_message_id', $inbox_value->message_id)->first();
                // dd($record);
                if (empty($record)) {
                    $input = [];
                    $input['type']              = 'Email';
                    $input['account_id']        = $accountId;
                    $input['subscription_id']   = $subscriptionId;
                    $input['number']            = $kb_account_email;
                    $input['incoming']          = 0;
                    $input['count']             = 1;
                    $input['notes']             = $inbox_value->subject;
                    $input['email_message_id']  = $inbox_value->message_id;
                    $input['date']              = date('Y-m-d H:i:s', strtotime($inbox_value->date));

                    Subscription::create($input);
                    
                    if($recordUsage){
                        // data array send to server
                        $dataArray = array(
                                        'subscriptionId'   => $input['subscription_id'],
                                        'unitUsageRecords' => array(array(
                                            'unitType'          => 'count',
                                            'usageRecords'      => array(array(
                                                'recordDate'        => date('Y-m-d', strtotime($inbox_value->date)),
                                                'amount'            => 1
                                            ))  
                                        ))
                                    );
                        $client = new Client();
                        $requestOptions['json'] = $dataArray;
                        $client->post($endpoint, $requestOptions);
                    }
                    
                    
                    /*try{
                        $client = new Client();
                        $requestOptions['json'] = $dataArray;
                        $response = $client->post($endpoint, $requestOptions);
                        $dataArray = [];
                    }catch(ClientException $e){

                        $response           = $e->getResponse();
                        $content            = json_decode($response->getBody()->getContents(), true);
                        $data['message']    = (!empty($content["message"])) ? $content["message"] : 'usage error.';
                        $data['counter'] = $counter;
                        $data['req'] = $requestOptions;

                        dd($data);
                    }*/
                    
                    return $input;
                }
                
            }
        }
        return []; 
    }

}
