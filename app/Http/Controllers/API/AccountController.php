<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Config;
use App\Subscription;

//FOR GUZZLE
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller
{

    public $successStatus   = 200;
    public $errorStatus     = 401;

	private $headers = [];

    public function __construct(){

        $this->headers['Accept']                = 'application/json';
        $this->headers['Content-Type']          = 'application/json';
        $this->headers['X-Killbill-ApiKey']     = env('API_KEY');
        $this->headers['X-Killbill-ApiSecret']  = env('API_SECRET');
        $this->headers['X-Killbill-CreatedBy']  = 'Fee Me Web';
    }

    
    /**
    GET ALL ACCOUNTS 
    **/
    public function index($offset, $limit, $param = false){

        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $credentials    = getUserFromToken($headers['token']);
            if($credentials){
                $endpoint      = env('API_BASE_URL').'/accounts/pagination?offset='.$offset.'&limit='.$limit;
                try{
                    
                    $client    = new Client();
                    $response  = $client->get($endpoint, ['auth'=> $credentials, 'headers'=> $this->headers]);
                    
                    $data['status']     = true;
                    $data['message']    = 'Record Found';
                    $data['data']               = json_decode($response->getBody()->getContents(), true);

                    $data['total_records']      = $response->getHeader('x-killbill-pagination-totalnbrecords')[0];
                    $data['next_page_url']      = @$response->getHeader('x-killbill-pagination-nextpageuri')[0];
                    $data['max_records_showing']= $response->getHeader('x-killbill-pagination-maxnbrecords')[0];
                    $data['current_offset']     = $response->getHeader('x-killbill-pagination-currentoffset')[0];
                    $data['next_offset']        = @$response->getHeader('x-killbill-pagination-nextoffset')[0];

                    if($param){
                        return $data['data'];
                    }else{
                        return response()->json(['result' => $data], $response->getStatusCode());
                    }

                }catch(ClientException $e){

                    $response           = $e->getResponse();
                    $data['status']     = false;
                    $msg                = json_decode($response->getBody()->getContents());
                    $data['message']    = ($msg) ? $msg->message : 'Some exception occured.';
                    // $data['data']       = '';
                    if($param){
                        return $data['data'] = '';
                    }else{
                        return response()->json(['result' => $data], $response->getStatusCode());
                    }
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }
    /**
    GET ALL ACCOUNTS WITH SUBSCRIPTIONS
    **/
    public function accountSubscriptions(){

    	$headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $credentials    = getUserFromToken($headers['token']);
            if($credentials){
                $endpoint      = env('API_BASE_URL').'/accounts/paginationwithbundles?offset=0&limit=1000000';
                try{
                    
                    $client    = new Client();
                    $response  = $client->get($endpoint, ['auth'=> $credentials, 'headers'=> $this->headers]);
                    
                    $data['status']     = true;
                    $data['message']    = 'Accounts Found.';
                    $data['data']       = json_decode($response->getBody()->getContents(), true);

                    foreach ($data['data'] as $key => $value) {
                        
                        foreach ($value["bundles"] as $bundle_key => $bundle_value) {
                            // dd($bundle_value);
                            $subscriptions = $data['data'][$key]["subscriptions"][$bundle_key] = @($bundle_value["subscriptions"][0])?$bundle_value["subscriptions"][0] : $bundle_value["subscriptions"];

                            //changing key names in that subscription
                            if (!empty($subscriptions)) {
                                unset($data['data'][$key]["subscriptions"][$bundle_key]["events"]);
                                unset($data['data'][$key]["subscriptions"][$bundle_key]["prices"]);
                                $data['data'][$key]["subscriptions"][$bundle_key]["product"]   = @$subscriptions["productName"];
                                $data['data'][$key]["subscriptions"][$bundle_key]["plan"]      = @$subscriptions["planName"];
                                $data['data'][$key]["subscriptions"][$bundle_key]["status"]    = @$subscriptions["state"];                                
                            }
                        }

                        unset($data['data'][$key]["bundles"]);
                        // return $subscriptions;
                        
                        
                    }

                    return response()->json(['result' => $data], $response->getStatusCode());

                }catch(ClientException $e){

                    $response           = $e->getResponse();
                    $data['status']     = false;
                    $msg                = json_decode($response->getBody()->getContents());
                    $data['message']    = ($msg) ? $msg->message : 'Some exception occured.';

                    return response()->json(['result' => $data], $this->errorStatus);
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    /**
    GET SINGLE ACCOUNT DETAIL 
    **/
    public function getSingleAccountDetail($accountId, $param = false){
        
        $endpoint = env('API_BASE_URL').'/accounts/'.$accountId;
        $headers  = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $credentials    = getUserFromToken($headers['token']);
            if($credentials){

                try{
                    
                    $client             = new Client();
                    $response           = $client->get($endpoint, ['auth'=> $credentials, 'headers'=> $this->headers]);

                    $data['status']     = true;
                    $data['message']    = 'Account record found successfully.';
                    $data['data']       = json_decode($response->getBody()->getContents(), true);
                    if($param){
                        return $data['data'];
                    }else{
                        $data["data"]["dob"] = ($data["data"]["dob"])? date('n/j/Y', strtotime($data["data"]["dob"])) : '';
                        return response()->json(['result' => $data], $response->getStatusCode());
                    }
                    
                }catch(ClientException $e){
                    
                    $response = $e->getResponse();
                    $data['status']     = false;
                    $data['message']    = json_decode($response->getBody()->getContents(), true);
                    $data['message']    = (!empty(json_decode($response->getBody()->getContents()))) ? json_decode($response->getBody()->getContents()) : 'Account not found.';
                    // $data['data']       = '';
                    if($param){
                        return $data['data'] = '';
                    }else{
                        return response()->json(['result' => $data], $response->getStatusCode());
                    }
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }
    public function getAccountPaymentMethod($accountId){
        
        $headers  = apache_request_headers();
        $endpoint = env('API_BASE_URL').'/accounts/'.$accountId.'/paymentMethods';
        if(array_key_exists('token', $headers)){
            $credentials    = getUserFromToken($headers['token']);
            if($credentials){

                try{
                    
                    $client             = new Client();
                    $response           = $client->get($endpoint, ['auth'=> $credentials, 'headers'=> $this->headers]);

                    $data['status']     = true;
                    $data['message']    = 'Payment methods found successfully.';
                    $data['data']       = json_decode($response->getBody()->getContents(), true);

                    return response()->json(['result' => $data], $response->getStatusCode());
                }catch(ClientException $e){
                    
                    $response = $e->getResponse();
                    $data['status']     = false;
                    $content            = json_decode($response->getBody()->getContents(), true);
                    $data['message']    = (!empty($content["message"])) ? $content["message"] : 'Payment methods not found.';

                    return response()->json(['result' => $data], $response->getStatusCode());
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    /**
    ADD ACCOUNT
    **/
    public function storePersonalDetail(Request $request){
        // return $request->dob;
        $validator = Validator::make($request->all(), [
            'firstName'         => 'required',
            // 'middleName'        => 'required',
            'lastName'          => 'required',
            'email'             => 'required|email',
            'addressLineOne'    => 'required',
            'addressLineTwo'    => 'required',
            'addressCity'       => 'required',
            // 'province'          => 'required',
            'country'           => 'required',
            'mobile'            => 'required|numeric',
            // 'notes'             => 'required',
            "title"             => 'sometimes|required',
            "dob"               => 'sometimes|required',
            "gender"            => 'sometimes|required',
            "nationality"       => 'sometimes|required',
            "idNumber"          => 'sometimes|required',
            "timeZone"          => 'sometimes|required',
            "currency"          => 'sometimes|required',
            "billingCycleDay"   => 'sometimes|required'

        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $currency  = ($request->currency)?:'ZAR';

        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $credentials    = getUserFromToken($headers['token']);
            if($credentials){

                $bodyData = [
                    "name"              => $request->firstName,
                    "firstNameLength"   => strlen($request->firstName),
                    "externalKey"       => (!empty($request->idNumber) ? $request->externalKey : ''),
                    "email"             => $request->email,
                    "address1"          => $request->addressLineOne,
                    "address2"          => $request->addressLineTwo,
                    "city"              => $request->addressCity,
                    "state"             => $request->province,
                    "country"           => $request->country,
                    "phone"             => $request->mobile,
                    "notes"             => $request->notes,
                    "currency"          => $currency,
                    "billCycleDayLocal" => $request->billingCycleDay,

                    "locale"            => "en",
                    "title"             => $request->title,
                    "middleName"        => $request->middleName,
                    "lastName"          => $request->lastName,
                    "dob"               => $request->dob,
                    "gender"            => $request->gender,
                    "nationality"       => $request->nationality,
                    "idNumber"          => $request->idNumber,
                    "landline"          => $request->landLine,
                    "other"             => $request->otherNumber,
                    "suburb"            => $request->addressSubrub,
                    "timeZone"          => $request->timeZone,
                    "uploadFile"        => "http://www.google.com"
                ];
                $requestOptions = [
                    'auth'  => $credentials,
                    'headers' => $this->headers,
                    'json' => $bodyData,
                ];

                // return $requestOptions;

                $endpoint = env('API_BASE_URL').'/accounts';
                
                try{
                    
                    $client = new Client();
                    $response = $client->post($endpoint, $requestOptions);  

                    $message = "Account created successfully.";
                    if ((int) $response->getStatusCode() === 201) { 
                        if ($response->hasHeader('location')) {
                            # code...
                            $locationHeader = $response->getHeader('location')[0];
                            $locationHeaderArray = explode('/', $locationHeader);
                            $accountId = end($locationHeaderArray);                           

                            /**************** ADDING TAGS ***************/
                            if(!empty($request->tags)){
                                $tagsReponse = $this->storeTags($accountId, $request->tags, $credentials);
                                $message .= $tagsReponse['message'];
                            }
                            /**************** ADDING TAGS ***************/

                            /**************** ADDING PAYMENT METHOD ***************/
                            
                            
                            $paymentResponse = $this->attachPaymentMethod($accountId, $credentials);
                            $message .= $paymentResponse['message'];
                            /**************** ADDING PAYMENT METHOD ***************/

                        }
                        $data['status'] = true;
                        $data['message'] = $message;
                        $data['data'] = $accountId;

                        return response()->json(['result' => $data], $response->getStatusCode());
                    } 
                    
                }catch(ClientException $e){
                    
                    $response = $e->getResponse();
                    $data['status'] = false;
                    $data['message'] = json_decode($response->getBody()->getContents())->message;
                    // $data['data'] = '';

                    return response()->json(['result' => $data], $response->getStatusCode());
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    // update account
    public function updateAccount($accountId, Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'firstName'         => 'required',
            // 'middleName'        => 'required',
            'lastName'          => 'required',
            'addressLineOne'    => 'required',
            'addressLineTwo'    => 'required',
            'addressCity'       => 'required',
            // 'province'          => 'required',
            'country'           => 'required',
            'mobile'            => 'required|numeric',
            // 'notes'             => 'required',
            "title"             => 'sometimes|required',
            "dob"               => 'sometimes|required',
            "gender"            => 'sometimes|required',
            "nationality"       => 'sometimes|required',
            "idNumber"          => 'sometimes|required',
            "timeZone"          => 'sometimes|required',
            "currency"          => 'sometimes|required',
            "billingCycleDay"   => 'sometimes|required'

        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        // $currency  = ($request->currency)?:'ZAR';
        $requested_data = (array) $request->all();
        // dd ($requested_data);
        foreach ($requested_data as $key => $value) {
            $requested_data[$key] = (empty($value))? '' : $value;
        }
        // return $requested_data;

        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);

            if($checkToken){

                $credentials    = getUserFromToken($headers['token']);
                // dd($requested_data);
                $bodyData = [
                    "accountId"             => $requested_data["accountId"],
                    "name"                  => $requested_data["firstName"],
                    "firstNameLength"       => strlen($requested_data["firstName"]),
                    "externalKey"           => $requested_data["externalKey"],
                    "email"                 => $requested_data["email"],
                    "billCycleDayLocal"     => $requested_data["billCycleDayLocal"],
                    "currency"              => $requested_data["currency"],
                    "parentAccountId"       => $requested_data["parentAccountId"],
                    "isPaymentDelegatedToParent"       => $requested_data["isPaymentDelegatedToParent"],
                    "paymentMethodId"       => $requested_data["paymentMethodId"],
                    "referenceTime"         => $requested_data["referenceTime"],
                    "timeZone"              => $requested_data["timeZone"],

                    "address1"              => $requested_data["addressLineOne"],
                    "address2"              => $requested_data["addressLineTwo"],
                    "postalCode"            => $requested_data["postalCode"],
                    "company"               => $requested_data["company"],
                    "city"                  => $requested_data["addressCity"],
                    "state"                 => $requested_data["province"],
                    "country"               => $requested_data["country"],
                    "locale"                => $requested_data["locale"],
                    "phone"                 => $requested_data["mobile"],
                    "notes"                 => $requested_data["notes"],
                    "isMigrated"            => $requested_data["isMigrated"],
                    "accountBalance"        => $requested_data["accountBalance"],
                    "accountCBA"            => $requested_data["accountCBA"],

                    "title"                 => $requested_data["title"],
                    "middleName"            => $requested_data["middleName"],
                    "lastName"              => $requested_data["lastName"],
                    "dob"                   => $requested_data["dob"],
                    "gender"                => $requested_data["gender"],
                    "nationality"           => $requested_data["nationality"],
                    "idNumber"              => $requested_data["idNumber"],
                    "landline"              => $requested_data["landLine"],
                    "other"                 => $requested_data["otherNumber"],
                    "suburb"                => $requested_data["addressSubrub"],
                    "uploadFile"            => $requested_data["uploadFile"]
                ];

                $requestOptions = [
                    'auth'  => $credentials,
                    'headers' => $this->headers,
                    'json' => $bodyData,
                ];
                
                $endpoint = env('API_BASE_URL').'/accounts/'.$accountId.'?treatNullAsReset=true';
               
                try {
                    $client = new Client();
                    $response = $client->put($endpoint, $requestOptions);

                    $responseData['status'] = true;
                    $responseData['message'] = "Account updated successfully.";

                    return response()->json(['result' => $responseData], 200);//because 204 send empty response data
                }
                catch (ClientException $e) {
                    $response           = $e->getResponse();
                    $errorData['status']     = false;
                    $msg                = json_decode($response->getBody()->getContents());
                    $errorData['message']    = ($msg) ? $msg->message : 'Some exception occured.';

                    return response()->json(['result' => $errorData]);
                }
                
            }else{
                $invalidData['status'] = false;
                $invalidData['message'] = "Invalid Token.";

                return response()->json(['result' => $invalidData], $this->errorStatus);
            }
        }else{
            $tokenData['status']     = false;
            $tokenData['message']    = "Token is required.";

            return response()->json(['result' => $tokenData], $this->errorStatus);
        }
    }

    /**************** COMPANY DETAIL ********************/
    public function storeCompanyDetail(Request $request){

        $validator = Validator::make($request->all(), [
            'companyName'           => 'required',
            'registeredName'        => 'required',
            'companyRegistrationNo' => 'required',
            'companyType'           => 'required',
            'industary'             => 'required',
            'vatRegistrationNo'     => 'required',
            'website'               => 'required',
            'billableDomain'        => 'required',
            'mobile'                => 'required|numeric',
            'addressLineOne'        => 'required',
            "addressLineTwo"        => 'required',
            "city"                  => 'required',
            "country"               => 'required',
            "province"              => 'required',
            // "notes"                 => 'required',
            "accountId"             => 'required'

        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $userCred = getUserFromToken($request->header('token'));
        $this->credential[0] = $username = $userCred[0];
        $this->credential[1] = $password = $userCred[1];

        // return $accountId;
       $accountId = $request->accountId;
       $endpoint1 = env('API_BASE_URL')."/accounts/company/$accountId";
       $endpoint2 = env('API_BASE_URL')."/accounts/$accountId";

       $bodyData2 = ["company"    => $request->companyName];
       $bodyData1 = [
                    "companyRegisteredName"     => $request->registeredName,
                    "companyRegistrationNumber" => $request->companyRegistrationNo,
                    "companyType"               => $request->companyType,
                    "companyIndustry"           => $request->industary,
                    "companyVATRegistrationNumber" => $request->vatRegistrationNo,
                    "companyWebsite"            => $request->website,
                    "companyBillableDomain"     => $request->billableDomain,
                    "companyMobile"             => $request->mobile,
                    "companyLandLine"           => $request->landLine,
                    "companyOther"              => $request->otherPhone,
                    "companyAddress1"           => $request->addressLineOne,
                    "companyAddress2"           => $request->addressLineTwo,
                    "companySuburb"             => $request->subrub,
                    "companyCity"               => $request->city,
                    "companyCountry"            => $request->country,
                    "companyProvince"           => $request->province,
                    "companyNotes"              => $request->notes,
                    "companyUploadFile"         => "http://www/change-this-in-future.com"
        ];
        
        $requestOptions1 = [
                'auth'  => [$username, $password],
                'headers' => $this->headers,
                'json' => $bodyData1,
             ];
        $requestOptions2 = [
                'auth'  => [$username, $password],
                'headers' => $this->headers,
                'json' => $bodyData2,
             ];
        // return $requestOptions;
        try{
            $client = new Client();
            $response = $client->post($endpoint1, $requestOptions1);
            $response2 = $client->put($endpoint2, $requestOptions2);
            $message = "Company detail added successfully.";
            
            $data['status'] = true;
            $data['message'] = $message;
            // $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
            
        }catch(ClientException $e){
            $response = $e->getResponse();
            $data['status'] = false;
            $content = json_decode($response->getBody()->getContents(), true);
            $data['message'] = ($content["message"])? $content["message"]: "Company detail not stored.";
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }

    // update company details
    public function updateCompanyDetail($accountId, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'companyName'           => 'required',
            'registeredName'        => 'required',
            'companyRegistrationNo' => 'required',
            'companyType'           => 'required',
            'industary'             => 'required',
            'vatRegistrationNo'     => 'required',
            'website'               => 'required',
            'billableDomain'        => 'required',
            'mobile'                => 'required|numeric',
            'addressLineOne'        => 'required',
            "addressLineTwo"        => 'required',
            "city"                  => 'required',
            "country"               => 'required',
            "province"              => 'required',
            // "notes"                 => 'required',
            "accountId"             => 'required'

        ]);

        if ($validator->fails()) {
            
            $data['status'] = false;
            $data['message'] = $validator->errors();

            return response()->json(['result'=>$data], $this->errorStatus);            
        }

        $headers = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $token = $headers['token'];
            $checkToken = getUserFromToken($token);

            if($checkToken){

                $credentials    = getUserFromToken($headers['token']);

                $endpoint1 = env('API_BASE_URL')."/accounts/company/$accountId";
                $endpoint2 = env('API_BASE_URL')."/accounts/$accountId";

                $bodyData1 = [
                            "companyRegisteredName"     => $request->registeredName,
                            "companyRegistrationNumber" => $request->companyRegistrationNo,
                            "companyType"               => $request->companyType,
                            "companyIndustry"           => $request->industary,
                            "companyVATRegistrationNumber" => $request->vatRegistrationNo,
                            "companyWebsite"            => $request->website,
                            "companyBillableDomain"     => $request->billableDomain,
                            "companyMobile"             => $request->mobile,
                            "companyLandLine"           => $request->landLine,
                            "companyOther"              => $request->otherPhone,
                            "companyAddress1"           => $request->addressLineOne,
                            "companyAddress2"           => $request->addressLineTwo,
                            "companySuburb"             => $request->subrub,
                            "companyCity"               => $request->city,
                            "companyCountry"            => $request->country,
                            "companyProvince"           => $request->province,
                            "companyNotes"              => $request->notes,
                            "companyUploadFile"         => "http://www/change-this-in-future.com"
                ];
                // return $bodyData1;
                $bodyData2 = ["company"    => $request->companyName];

                /*$requestOptions = [
                    'auth'  => $credentials,
                    'headers' => $this->headers,
                    'json' => $bodyData,
                ];*/

                $requestOptions1 = [
                        'auth'  => $credentials,
                        'headers' => $this->headers,
                        'json' => $bodyData1,
                     ];
                $requestOptions2 = [
                        'auth'  => $credentials,
                        'headers' => $this->headers,
                        'json' => $bodyData2,
                     ];
               
                try {
                    $client = new Client();
                    $client2 = new Client();
                    $response = $client->put($endpoint1, $requestOptions1);  
                    $response2 = $client2->put($endpoint2, $requestOptions2);
                    
                    if ($response->getStatusCode() >= 200 && $response->getStatusCode() <=299) { 

                        $data['status'] = true;
                        $data['message'] = "Company details updated successfully.";

                        return response()->json(['result' => $data], $response->getStatusCode());
                    } 
                }
                catch (ClientException $e) {
                    $response           = $e->getResponse();
                    $data['status']     = false;
                    $msg                = json_decode($response->getBody()->getContents());
                    $data['message']    = ($msg) ? $msg->message : 'Some exception occured.';
                    // $data['data']       = '';

                    return response()->json(['result' => $data], $response->getStatusCode());
                }
                
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }
    /**************** COMPANY DETAIL ********************/

    /**************** BILLING DETAIL ********************/
    function storeBillingDetail(Request $request, $accountId){
        $userCred = getUserFromToken($request->header('token'));
        $this->credential[0] = $username = $userCred[0];
        $this->credential[1] = $password = $userCred[1];

        $endpoint = env('API_BASE_URL')."/accounts/$accountId?treatNullAsReset=false";
       $bodyData = [
                    // "billCycleDayLocal"     => $request->billingCycleDay,
                    // "billingDomain"         => $request->billingDomain,
                    // "billingTime"           => $request->billingTime,
                    // "currency"              => $request->currency,
                    // "externalKey"           => $request->externalKey,
                    // "timeZone"              => $request->timeZone
        ];
        // return $endpoint;
        $requestOptions = [
                'auth'  => [$username, $password],
                'headers' => $this->headers,
                'json' => $bodyData,
             ];
        try{
            $client = new Client();
            $response = $client->put($endpoint, $requestOptions);
            $message = "Account updated successfully";
            
            $data['status'] = true;
            $data['message'] = $message;
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
            
        }catch(ClientException $e){
            $response = $e->getResponse();
            $data['status'] = false;
            $content = json_decode($response->getBody()->getContents(), true);
            $data['message'] = ($content["message"])? $content["message"]: "Account updation failed.";
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }
    /**************** BILLING DETAIL ********************/

    /**************** STORE TAGS ********************/
    public function storeTags($accountId, $tags, $credential){
        $endpoint = env('API_BASE_URL').'/tagDefinitions';
        
        $tagsDefinition = [];
        try{
            // return 'hello';
            $client = new Client(); //
            $response = $client->get( $endpoint, ['auth'=> $credential, 'headers'=> $this->headers]);
            $response = json_decode($response->getBody()->getContents());

            /************* MAKING ARRAY OF ALL AVAILABLE SERVER TAGS ***************/
            foreach ($response as $key => $value) {
                $tagsDefinition[$value->name] = $value->id;
            }
            /************* MAKING ARRAY OF ALL AVAILABLE SERVER TAGS ***************/
            
            /***************** GETTING TAGS IDS *************************/
            $tagList = [];
            /*print_r($tags);
            print_r($tagsDefinition);*/
            foreach ($tags as $key => $value) {
                $tagList[] = $tagsDefinition[$value['name']];
            }
            /***************** GETTING TAGS IDS *************************/
            // return implode(',', $tagList);


            /***************** ATTACHING TAGS TO ACCOUNT ***********************/
            $requestOptions = [
                'auth'  => $credential,
                'headers' => $this->headers
             ];
            $tagListString = implode(',', $tagList);
            $addTagEndPoint = env('API_BASE_URL')."/accounts/{$accountId}/tags?tagList=".$tagListString;
            // return $addTagEndPoint;
            $postResponse = $client->post($addTagEndPoint, $requestOptions);

            if ($postResponse->getStatusCode() === 201) {
                $data['status'] = true;
                $data['message'] = ", Tag/s added to account.";
                return $data;
            }


        }catch(ClientException $e){
            $response = $e->getResponse();
            $data['status'] = false;
            $data['message'] = ", Tag/s not added to account.";

            return $data;
        }/*catch(GuzzleException $e){
            dd($e);

            return $data['message'];
        }*/
    }

    public function attachPaymentMethod($accountId, $credential){
        $endpointPayment = env('API_BASE_URL')."/accounts/$accountId/paymentMethods?isDefault=true";
        $paymentBodyData = [
                            "pluginName" => "__EXTERNAL_PAYMENT__"
                            ];
        $paymentOptions = [
                            'auth'  => $credential,
                            'headers' => $this->headers,
                            'json' => $paymentBodyData,
                        ];
        // return $paymentOptions;
        try{
            $client = new Client(); 
            $postResponse = $client->post($endpointPayment, $paymentOptions);

            if ($postResponse->getStatusCode() === 201) {
                $data['status'] = true;
                $data['message'] = ", Payment method attched to account.";
                return $data;
            }


        }catch(ClientException $e){
            $response = $e->getResponse();
            $data['status'] = false;
            $data['message'] = ", Payment method not attched.";

            return $data;
        }
    }
    
    /**************** CLOSE ACCOUNT ********************/
    public function destroy($account_id){
        $headers = apache_request_headers();
        if(!array_key_exists('token', $headers)){
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
        $userCred = getUserFromToken($headers["token"]);

        $endpoint = env('API_BASE_URL').'/accounts/'.$account_id.'?cancelAllSubscriptions=true&writeOffUnpaidInvoices=true&itemAdjustUnpaidInvoices=true';
        try{
            $client = new Client();
            $response = $client->delete($endpoint, ['auth'=> $userCred, 'headers'=> $this->headers]);
            $data['status'] = true;
            $data['data'] = '';
            $data['message'] = 'Account blocked successfully.';
            if ($response->getStatusCode() === 200) {
                return response()->json(['result' => $data], $response->getStatusCode());
            }
            
        }catch(ClientException $e){
            $response = $e->getResponse();
            $data['status'] = false;
            $content = json_decode($response->getBody()->getContents(), true);
            $data['message'] = ($content["message"])? $content["message"]: "Account blocking failed.";
            $data['data'] = '';

            return response()->json(['result' => $data], $response->getStatusCode());
        }
    }

    // company details
    public function getCompanyDetails($accountId){
        
        $endpoint = env('API_BASE_URL').'/accounts/company/'.$accountId.'?accountWithBalance=true&accountWithBalanceAndCBA=true&audit=FULL';
        
        $headers  = apache_request_headers();

        if(array_key_exists('token', $headers)){
            $credentials    = getUserFromToken($headers['token']);
            if($credentials){

                try{
                    
                    $client             = new Client();
                    $response           = $client->get($endpoint, ['auth'=> $credentials, 'headers'=> $this->headers]);

                    $data['status']     = true;
                    $data['message']    = 'Company details found successfully.';
                    $data['data']       = json_decode($response->getBody()->getContents(), true);
                    
                    return response()->json(['result' => $data], $response->getStatusCode());
                    
                }catch(ClientException $e){
                    
                    $response = $e->getResponse();
                    $data['status']     = false;
                    $data['message']    = json_decode($response->getBody()->getContents(), true);
                    $data['message']    = (!empty(json_decode($response->getBody()->getContents()))) ? json_decode($response->getBody()->getContents()) : 'Account not found.';
                    
                    return response()->json(['result' => $data], $response->getStatusCode());
                }
            }else{
                $data['status'] = false;
                $data['message'] = "Invalid Token.";

                return response()->json(['result' => $data], $this->errorStatus);
            }
        }else{
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
    }

    public function allocateAttendance(Request $request){

        $headers = apache_request_headers();
        // return $headers;
        if(!array_key_exists('token', $headers)){
            $data['status']     = false;
            $data['message']    = "Token is required.";

            return response()->json(['result' => $data], $this->errorStatus);
        }
        $credentials    = getUserFromToken($headers['token']);
        if(empty($credentials)){
            $data['status'] = false;
            $data['message'] = "Invalid Token.";

            return response()->json(['result' => $data], $this->errorStatus);
        }


        $accountId = $request->accountId;
        $type = strtolower($request->type);
        $tableId = $request->tableId;
        // return Config::get("feemeconstants.feeme_unit.$type");

        $accountsubscriptions = app('App\Http\Controllers\API\SubscriptionController')->getAccountBundle($accountId, true);
        // return $accountsubscriptions;
        $subscriptions_data = array_column($accountsubscriptions, 'subscriptions');
        // return $subscriptions_data;
        // echo $type;dd();
        
        foreach ($subscriptions_data as $keyMain => $subscriptions) {
            foreach ($subscriptions as $key => $value) {
                $type2 = '';
                if((string)$type === 'email'){
                    $type2 = 'emails';
                }
                $type3 = '';
                if((string)$type === 'travel'){
                    $type3 = 'travel-time';
                }
                $productName = strtolower($value["productName"]);
                $state = strtoupper($value["state"]);
                // echo $productName;
                if (((string)$type === (string)$productName || (string)$productName === $type2 || (string)$productName === $type3) && $state == "ACTIVE") {
                    
                    // return $type;
                    //get subscription id
                    //update table 
                    //record usage
                    $subscriptionId = $value["subscriptionId"];
                    $accountId = $value["accountId"];
                    // return $value;
                    // return 'tested';
                    $record = Subscription::where('id', $tableId)->first();
                    if (!empty($record)) {
                        $record->account_id = $accountId;
                        $record->subscription_id = $subscriptionId;
                        

                        $unit       = Config::get("feemeconstants.feeme_unit.$type");
                        $recordDate = date('Y-m-d', time());
                        switch ($productName) {
                            case 'call':
                            case 'meeting':
                            case 'travel':
                            case 'travel-time':
                                $amount = $record->duration;
                            break;
                            
                            case 'sms':
                            case 'email':
                            case 'emails':
                                $amount = $record->count;
                            break;
                        }

                        // return $amount;
                        //record usage
                        $result = $this->recordUsage($credentials, $subscriptionId, $unit, $recordDate, $amount);

                        if ($result) {
                            $record->save();
                            $data['status'] = true;
                            $data['message'] = "Record allocated and usage recorded.";
                            return response()->json(['result' => $data], 200);
                        }else{
                            $data['status'] = false;
                            $data['message'] = "Attendance not allocated.";
                            return response()->json(['result' => $data], 400);
                        }
                    }
                    break;
                }
            }
        }
        $data['status'] = false;
        $data['message'] = "Attendance not allocated, please check if account have subscribed this product.";
        return response()->json(['result' => $data], 404);
    }
    private function recordUsage($credentials, $subscriptionId, $unit, $recordDate, $amount){

        $endpoint       = env('API_BASE_URL').'/usages';
        $requestOptions = [
                            'auth'  => $credentials,
                            'headers' => $this->headers,
                            'json' => "",
                        ];

        $dataArray = array(
                            'subscriptionId'   => $subscriptionId,
                            'unitUsageRecords' => array(array(
                                'unitType'          => $unit,
                                'usageRecords'      => array(array(
                                    'recordDate'        => $recordDate,
                                    'amount'            => $amount
                                ))  
                            ))
                        );

        try{
            $client = new Client();
            $requestOptions['json'] = $dataArray;
            $response = $client->post($endpoint, $requestOptions);
            if ($response->getStatusCode() > 199 || $response->getStatusCode() < 299) {
                return true;
            }

        }catch(ClientException $e){
            return false;
        }

        return false;
    }

}
