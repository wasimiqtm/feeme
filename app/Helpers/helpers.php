<?php

use App\User;

if (! function_exists('create_user')) {
    
    function create_user($encoded_data)
    {   
        $token        = str_random('20');
        $data['encoded_data'] = $encoded_data;
        $data['token'] = $token;

        $user = User::Create(['encoded_data' => $encoded_data, 'token' => $token]);
        // $user = User::updateOrCreate(['encoded_data' => $encoded_data], ['token' => $token]);
        return $token;
    } 
}

if (! function_exists('update_user_password')) {
    
    function update_user_password($token, $encoded_data)
    {   
        $user           = User::where('token' , $token)->firstOrFail();
        $data['encoded_data'] = $encoded_data;

        $user->update($data);
        return true;
    } 
}

if (! function_exists('getUserFromToken')) {
    
    function getUserFromToken($token)
    {   
        if (!empty($token)) {
            $user           = \DB::table('users')->where('token' , $token)->pluck('encoded_data')->first();
            if($user)
            {
                $info           = base64_decode($user);
                $data           = explode(':', $info);
                $finalArray[]   = $data[0];
                $finalArray[]   = $data[1];

                return $finalArray;
            }else{
                return false;
            }
        }
        return false;
    } 
}



